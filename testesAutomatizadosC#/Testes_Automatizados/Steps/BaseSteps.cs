﻿using BDD.Util.Banco;
using BDD.Util.Massa_Dados;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Testes_Automatizados.Common;
using Testes_Automatizados.Page;

namespace Testes_Automatizados.Steps.PreEfetivaSteps
{
    [Binding]
    public class BaseSteps 
    {
        private readonly GeralPage page;
        public BaseSteps(GeralPage page) 
        {
            this.page = page;
        }

        [Given(@"que eu inicie uma proposta do parceiro '(.*)'")]
        public void DadoQueEuInicieUmaPropostaDoParceiro(string p0)
        {
            switch (p0)
            {
                case "Losango":
                    page.geraProposta = new PropostaLosango();
                    break;
                case "DaCasa":
                    page.geraProposta = new PropostaDaCasa();
                    break;
                default:
                    page.geraProposta = new GeraProposta();
                    break;
            }
        }


        [Given(@"que eu inicie uma proposta com parceiro'(.*)' e com os seguintes valores '(.*)','(.*)',(.*),'(.*)','(.*)','(.*)'")]
        public async Task DadoQueEuInicieUmaPropostaComParceiroEComOsSeguintesValores(string p0, string p1, string p2, string p3, string p4, string p5, string p6)
        {
            var prazo = Convert.ToInt32(p1);
            var valorcompra = Convert.ToDecimal(p2);
            var seguro = p3;
            var dataprimeirovencimento = p4;
            var plano = p5;
            var produto = p6;

            switch (p0)
            {
                case "Losango":
                    page.geraProposta = new PropostaLosango();
                    await page.geraProposta.novosValores(prazo, valorcompra, seguro, dataprimeirovencimento, plano, produto);
                    break;
                //case "DaCasa":
                //    page.geraProposta = new PropostaDaCasa();
                //    break;
                //default:
                //    page.geraProposta = new GeraProposta();
                //    break;
            }

        }

        [Given(@"a proposta esteja ""(.*)""")]
        public void DadoAPropostaEsteja(string p0)
        {
            page.geraProposta.propostasCredito["SITUACAO"] = page.situacoes[p0];
        }

        [Given(@"utilize o produto '(.*)'")]
        public void DadoUtilizeOProduto(int p0)
        {
            page.geraProposta.propostasCredito["PRODUTO"] = p0;
        }

        [Given(@"grave essa proposta")]
        public void DadoGraveEssaProposta()
        {
            page.geraProposta.insereProposta();
            page.proposta = page.geraProposta._proposta;
        }

        [Given(@"prepare a chamada da '(.*)'")]
        public void DadoPrepareAChamadaDa(string p0)
        {
            page.proposta = page.geraProposta._proposta;
            page.loja = page.geraProposta.propostasCredito["LOJA"];
            page.lojista = page.geraProposta.propostasCredito["LOJISTA"];
            page.parceiro = page.geraProposta.propostasCredito["PARCEIRO_ID"];
            page.usuario = page.geraProposta.propostasCredito["USUARIO"];
            switch (p0)
            {
                case "Losango":
                    page.DadosPropostaLosango(page.proposta);
                    page.SaveVariavelfixa("proposta", page.proposta);
                    break;
                case "DaCasa":
                    page.DadosPropostaDACASA(page.proposta);
                    page.SaveVariavelfixa("proposta", page.proposta);
                    break;
                default:
                    page.geraProposta.insereProposta();
                    page.SaveVariavelfixa("proposta", page.proposta);
                    break;
            }
        }

        [Then(@"retorna o codigo (.*)")]
        public void EntaoRetornaOCodigo(int p0)
        {
            page.ReturnCode(p0);
        }

        [Then(@"retorna a mensagem '(.*)'")]
        public void EntaoRetornaAMensagem(string p1)
        {
            string p0 = "/mensagem";
            page.ComparaSeIgual(p0,p1);
        }

        [Then(@"consulto no banco a situação da proposta pré-efetivada")]
        public void EntaoConsultoNoBancoASituacaoDaPropostaPre_Efetivada()
        {
            page._GetStoreProcedure("USP_BUSCAPROPOSTA", new Dictionary<string, object>
            {
                { "@PROPOSTA", page.proposta  },
                { "@EMPRESA", "01" }
            });
            var teste = JsonConvert.DeserializeObject(page.respDB);
            var retorno = JArray.FromObject(teste);
            Assert.AreEqual("40", retorno[0]["Situacao"].ToString());
        }

        [Then(@"retorna a mensagem de Erro '(.*)'")]
        public void EntaoRetornaAMensagemDeErro(string p0,string p1)
        { 
            page.ComparaSeIgual(p0,p1);
        }

        [Then(@"retorna o aviso '(.*)'")]
        public void EntaoRetornaOAviso(string p0)
        {
            page.ComparaAvisoIgual(p0);
        }



        [Given(@"que eu tenha uma propostas Pré-Efetivada")]
         public void DadoQueEuTenhaUmaPropostasPre_Efetivada()
        {
            var geraProposta = new PropostaLosango();
            geraProposta.propostasCredito["SITUACAO"] = 40;
            geraProposta.insereProposta();

            page.proposta = geraProposta._proposta;

            page.loja = geraProposta.propostasCredito["LOJA"];
            page.lojista = geraProposta.propostasCredito["LOJISTA"];
            page.parceiro = geraProposta.propostasCredito["PARCEIRO_ID"];
            page.usuario = geraProposta.propostasCredito["USUARIO"];

            page.DadosPropostaLosango(page.proposta);
        }

        [When(@"realizo uma Pré-Efetivação")]
        public void QuandoRealizoUmaPre_Efetivacao()
        {
            page.PostPreEfetiva(page.body.Trim());
        }
        [When(@"realizo uma Efetivação")]
        public void QuandoRealizoUmaEfetivacao()
        {
            page.PostEfetiva(page.body.Trim());
        }




        [When(@"insiro a garantia com o chassi '(.*)'")]
        public void QuandoInsiroAGarantiaComOChassi(string p0)
        {
            page.PostGarantiaChassi(p0);
        }

        [Then(@"limpo as variaveis")]
        public void EntaoLimpoAsVariaveis()
        {
            page.LimpaVariaveis();
        }

     
        [When(@"insiro a garantia")]
        public void QuandoInsiroAGarantia()
        {

            page.PostGarantia();
        }

        [Then(@"preparo para a efetivação")]
        public void EntaoPreparoParaAEfetivacao()
        {
            page.DadosPropostaEfetivacaoLosango(page.proposta);
            page._PreparaBodyEfetivacao();
        }



        [Then(@"verifico se os regitros contabeis foram gravados")]
        public void EntaoVerificoSeOsRegitrosContabeisForamGravados()
        {
            page._GetStoreProcedure("USP_EXISTELANCAMENTOGARANTIA", new Dictionary<string, object>
            {
                { "@PROPOSTA", page.proposta}
            });

            var json = (JArray)JsonConvert.DeserializeObject(page.respDB);

            Console.WriteLine(json);
        }




    }
}


