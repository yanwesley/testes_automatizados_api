﻿using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using Testes_Automatizados.Page;

namespace Testes_Automatizados.Steps.PreEfetivaSteps
{
    [Binding]
    public class ValidaChaveSteps
    {
        private readonly GeralPage page;
        private Dictionary<string, string> situacoes = new Dictionary<string, string>
        {
            { "PRONTA PARA PRE EFETIVAR","40" },
            { "PRE EFETIVADA","21" },
        };

        public ValidaChaveSteps(GeralPage page)
        {
            this.page = page;
        }

        
        [Given(@"altero a data nascimento para '(.*)'")]
        public void DadoAlteroADataNascimentoPara(string p0)
        {
            if (p0 == "nulo")
                page.geraProposta.clientes["NASCIMENTO"] = null;

            else
                page.geraProposta.clientes["NASCIMENTO"] = p0;
        }

        [Given(@"altero a Agencia e deixo invalida")]
        public void DadoAlteroAAgenciaEDeixoInvalida()
        {
            page.geraProposta.propostasCredito["AGENCIA"] = "0000";
            page.geraProposta.propostasOperacoes["AGENCIA"] = "0000";

        }
    }
}
