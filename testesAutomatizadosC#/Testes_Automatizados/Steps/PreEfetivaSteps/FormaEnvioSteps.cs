﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using TechTalk.SpecFlow;
using Testes_Automatizados.Page;

namespace Testes_Automatizados
{
    [Binding]
    public class FormaEnvioSteps
    {

        private readonly GeralPage page;
        public FormaEnvioSteps(GeralPage page)
        {
            this.page = page;
        }
        
      
        [Given(@"forma de envio seja '(.*)'")]
        public void DadoFormaDeEnvioSeja(string p0)
        {
            var body = JObject.Parse(page.body);

            switch (p0)
            {    

                case "correio":
                    body["formaEnvio"]["correio"] = "S";
                    body["formaEnvio"]["email"] = "N";
                    body["formaEnvio"]["loja"] = "N";
                    page.body = JsonConvert.SerializeObject(body);
                    break;
                case "email":
                    body["formaEnvio"]["correio"] = "N";
                    body["formaEnvio"]["email"] = "S";
                    body["formaEnvio"]["loja"] = "N";
                    page.body = JsonConvert.SerializeObject(body);
                    break;
                case "loja":
                    body["formaEnvio"]["correio"] = "N";
                    body["formaEnvio"]["email"] = "N";
                    body["formaEnvio"]["loja"] = "S";
                    page.body = JsonConvert.SerializeObject(body);
                    break;
                default:
                    break;
            }

         
        }

        [Given(@"endereço de e-mail esteja preenchido como '(.*)'")]
        public void DadoEnderecoDeE_MailEstejaPreenchidoComo(string p0)
        {
            var body = JObject.Parse(page.body);
            body["formaEnvio"]["endereco_email"] = p0;
            page.body = JsonConvert.SerializeObject(body);

        }



    }
}
