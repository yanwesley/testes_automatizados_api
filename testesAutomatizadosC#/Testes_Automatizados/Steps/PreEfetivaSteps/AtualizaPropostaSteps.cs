﻿using BDD.Util.Banco;
using BDD.Util.Massa_Dados;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using TechTalk.SpecFlow;
using Testes_Automatizados.Page;

namespace Testes_Automatizados.Steps.PreEfetivaSteps
{
    [Binding]
    public class AtualizaPropostaSteps
    {
        private readonly SqlServerContext conexao;
        private readonly GeralPage page;
        private Dictionary<string, string> situacoes = new Dictionary<string, string>
        {
            { "PRONTA PARA PRE EFETIVAR","40" },
            { "PRE EFETIVADA","21" },
        };

        public AtualizaPropostaSteps(IConfiguration configuration)
        {
            conexao = new SqlServerContext(configuration);
            
            page = new GeralPage();
        }

        [Then(@"valido que as informações do seguro persisitiram no banco")]
        public void EntaoValidoQueAsInformacoesDoSeguroPersisitiramNoBanco()
        {
            //@PROPOSTA CHAR(10), @AGENCIA CHAR(4), @EMPRESA CHAR(2)
            page.GetStoreProcedure("USP_BUSCAGRAVASEGURO", new Dictionary<string, object>
            {
                { "@EMPRESA","01"},
                { "@PROPOSTA", page.proposta},
            });

            Assert.IsTrue(page.respDB.Contains(page.proposta));

        }

    }
}


