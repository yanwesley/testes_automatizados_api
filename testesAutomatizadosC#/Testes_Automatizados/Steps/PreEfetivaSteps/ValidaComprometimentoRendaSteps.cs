﻿using BDD.Util.Banco;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using Testes_Automatizados.Page;

namespace Testes_Automatizados.Steps.PreEfetivaSteps
{
    [Binding]
    public class ValidaComprometimentoRendaSteps
    {
        private readonly GeralPage page;
        private Dictionary<string, string> situacoes = new Dictionary<string, string>
        {
            { "PRONTA PARA PRE EFETIVAR","40" },
            { "PRE EFETIVADA","21" },
        };

        public ValidaComprometimentoRendaSteps(GeralPage page)
        {
            this.page = page;
        }


        [Given(@"com o valor de renda igual ao valor solicitado")]
        public void DadoComOValorDeRendaIgualAoValorSolicitado()
        {

            page.geraProposta.clientes["RENDAAPROVADA"] = page.geraProposta._simulacao["valorCompra"];
            
        }



    }
}
