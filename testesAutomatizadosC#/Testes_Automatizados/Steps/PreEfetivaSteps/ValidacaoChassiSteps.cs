﻿using BDD.Util.Banco;
using BDD.Util.Massa_Dados;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using Testes_Automatizados.Page;

namespace Testes_Automatizados.Steps.PreEfetivaSteps
{
    [Binding]
    public class ValidacaoChassiSteps
    {
        private readonly SqlServerContext conexao;
        private readonly GeralPage page;
        private Dictionary<string, string> situacoes = new Dictionary<string, string>();


        public ValidacaoChassiSteps(GeralPage page)
        {
            this.page = page;
        }

        [Given(@"que eu tenha uma proposta pronta para ser pré-efetivada com produto '(.*)'")]
        public void DadoQueEuTenhaUmaPropostaProntaParaSerPre_EfetivadaComProduto(string p0)
        {


            page.geraProposta._produto = p0;

            page.geraProposta.insereProposta();

            page.proposta = page.geraProposta._proposta;

            page.loja = page.geraProposta.propostasCredito["LOJA"];
            page.lojista = page.geraProposta.propostasCredito["LOJISTA"];
            page.parceiro = page.geraProposta.propostasCredito["PARCEIRO_ID"];
            page.usuario = page.geraProposta.propostasCredito["USUARIO"];

            page.DadosPropostaLosango(page.proposta);
            page.SaveVariavelfixa("proposta", page.proposta);
        }

    }
}