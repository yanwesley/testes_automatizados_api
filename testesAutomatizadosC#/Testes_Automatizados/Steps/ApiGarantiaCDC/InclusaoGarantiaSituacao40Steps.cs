﻿using BDD.Util.Massa_Dados;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using Testes_Automatizados.Page;

namespace Testes_Automatizados.Steps.ApiGarantiaCDC
{
    [Binding]
    public class InclusaoGarantiaSituacao40Steps
    {
        private readonly GeralPage page;   

        public InclusaoGarantiaSituacao40Steps(GeralPage page)
        {
             this.page = page;
        }

        [Given(@"gero uma nova proposta e deixo na situacao '(.*)'")]
        public void DadoGeroUmaNovaPropostaEDeixoNaSituacao(int p0)
        {   

            page.geraProposta. insereProposta();

            page.proposta = page.geraProposta._proposta;

            page.loja = page.geraProposta.propostasCredito["LOJA"];
            page.lojista = page.geraProposta.propostasCredito["LOJISTA"];
            page.parceiro = page.geraProposta.propostasCredito["PARCEIRO_ID"];
            page.usuario = page.geraProposta.propostasCredito["USUARIO"];

            page.DadosPropostaDACASA(page.proposta);
            page.SaveVariavelfixa("proposta", page.proposta);


            page.respDB = null;
        }

        [Then(@"eu informo o valor '(.*)' no campo produto")]
        public void EntaoEuInformoOValorNoCampoProduto(string p0)
        {
            
            page.DadosProdutoAlterado(p0);
        }

        [Then(@"realizo uma Pré-Efetivação")]
        public void EntaoRealizoUmaPre_Efetivacao()
        {
            page.PostPreEfetiva(page.body.Trim());
        }

        [When(@"retorna o codigo (.*)")]
        public void QuandoRetornaOCodigo(int p0)
        {
            page.ReturnCode(p0);            
        }

        [When(@"a mensagem '(.*)'")]
        public void QuandoAMensagem(string p1)
        {
            string p0 = "/mensagem";
            page.ComparaSeIgual(p0, p1);

        }

        [Then(@"consulta proposta confirmando situacao em (.*)")]
        public void EntaoConsultaPropostaConfirmandoSituacaoEm(string p0)
        {
            page._GetStoreProcedure("usp_BuscaSituacaoProposta", new Dictionary<string, object>
            {
                { "@idProposta", page.proposta}                
            });

            var json = (JArray)JsonConvert.DeserializeObject(page.respDB);
            var situacao = json[0]["SITUACAO"].ToString();

            Console.WriteLine(json);

            Assert.AreEqual(p0, situacao);
        }

        [When(@"consulta os dados gravados da garantia")]
        public void QuandoConsultaOsDadosGravadosDaGarantia()
        {
            page._GetStoreProcedure("USP_BUSCAGARANTIACONTRATO", new Dictionary<string, object>
            {
                { "@CONTRATO", page.proposta}
            });

            Console.WriteLine(page.respDB);
        }
    }
}
