﻿IF NOT EXISTS (SELECT TOP 1 1 FROM sys.all_objects WHERE type = 'P' AND name = 'USP_CRIAPRIMEIROPELANCAMENTO')
BEGIN  
	EXEC('CREATE PROCEDURE DBO.USP_CRIAPRIMEIROPELANCAMENTO AS SELECT 1')
END
GO  
ALTER  PROCEDURE DBO.USP_CRIAPRIMEIROPELANCAMENTO( @PROPOSTA VARCHAR(10)) AS
BEGIN
  DECLARE @DATA DATETIME;

  SELECT @DATA = DATAMOVIMENTO FROM PARAMETROS WITH (NOLOCK)

  INSERT INTO PE_LANCAMENTOS ( EMPRESA,
                               AGENCIA,   
                               CONTRATO,   
                               PARCELA,   
                               DATAMOVIMENTO,   
                               SEQUENCIA,   
                               HISTORICO,   
                               [DATA],   
                               DIRECIONAMENTO,   
                               GRUPO,   
                               MOEDA,   
                               VALOR,
                               DATAFINANCEIRA,   
                               SITUACAO )
  VALUES ( '01'
         , '0001'
         , @PROPOSTA
         , '000'
         , @DATA
         , '001'
         , '0001'
         , @DATA
         , 'EMPR'
         , 1
         , '01'
         , 1000.00
         , @DATA
         , 'A' )
END