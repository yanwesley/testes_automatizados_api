﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BDD.Util.Massa_Dados
{
    public class Massa_json
    {
        public string jsonCheque6Parcelas()
        {
            string body = @"{
                              'dadosLiberacao': {
                                'formaLiberacao': '2',
                                'cpf_cnpj': '92127483170',
                                'beneficiario': 'Youdu Udonta',
                                'banco': '001',
                                'agencia': '0102',
                                'tipocontabancaria': '1',
                                'conta': '00225568'
                              },
                              'formaEnvio': {
                                'correio': 'N',
                                'email': 'N',
                                'loja': 'S',
                                'endereco_email': 'teste@tes.com'
                              },
                              'tipoEfetivacao': {
                                'tipoEfetivacao': '2'
                              },
                              'cheques': [
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '1'
                                },
	                            {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '2'
                                },
	                            {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '3'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '4'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '5'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '6'
                                },
        {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '7'
                                },
        {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '8'
                                },
        {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '9'
                                }
                              ],
                              'dadosAdicionais': [
                                {
                                  'campo': '',
                                  'valor': ''
                                }
                              ],
                                'parceiro': '1',
                                'usuario': 'LOSANGO1',
                                'senha': '123456',
                                'lojista': '905166',
                                'loja': '5203',
                            }";

            return body;

        }

        public string jsonOkDACASA()
        {
            return @"{
                  'dadosLiberacao': {
                    'formaLiberacao': '2',
                    'cpf_cnpj': '01744236089',
                    'beneficiario': 'Youdu Udonta',
                    'banco': '001',
                    'agencia': '0102',
                    'tipocontabancaria': '1',
                    'conta': '00225568'
                  },
                  'formaEnvio': {
                    'correio': 'S',
                    'email': 'N',
                    'loja': 'S',
                    'endereco_email': 'teste@tes.com'
                  },
                  'tipoEfetivacao': {
                    'tipoEfetivacao': '2'
                  },
                  'cheques': [
                    {
                      'cmc7': '001262680108501145833000877341',
                      'parcela': '1'
                    },
                    {
                      'cmc7': '001262680108501145833000877341',
                      'parcela': '2'
                    },
                    {
                      'cmc7': '001262680108501145833000877341',
                      'parcela': '3'
                    },
                    {
                      'cmc7': '001262680108501145833000877341',
                      'parcela': '4'
                    },
                    {
                      'cmc7': '001262680108501145833000877341',
                      'parcela': '5'
                    }
                  ],
                  'dadosAdicionais': [
                    {
                      'campo': 'A_tipoSistema',
                      'valor': 'MESA'
                    }
                  ],
                  'parceiro': '25',
                  'usuario': 'DACASA1',
                  'senha': '123456',
                  'lojista': '905161',
                  'loja': '5169',
                  'proposta': '3809778357'
                }
                        ";
        }

        public string jsonOk()
        {
            string body = @"{
                              'dadosLiberacao': {
                                'formaLiberacao': '2',
                                'cpf_cnpj': '01744236089',
                                'beneficiario': 'Youdu Udonta',
                                'banco': '001',
                                'agencia': '0102',
                                'tipocontabancaria': '1',
                                'conta': '00225568'
                              },
                              'formaEnvio': {
                                'correio': 'S',
                                'email': 'N',
                                'loja': 'S',
                                'endereco_email': 'teste@tes.com'
                              },
                              'tipoEfetivacao': {
                                'tipoEfetivacao': '2'
                              },
                              'cheques': [
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '1'
                                },
	                            {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '2'
                                },
	                            {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '3'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '4'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '5'
                                }
                              ],
                              'dadosAdicionais': [
                                {
                                  'campo': 'A_tipoSistema',
                                  'valor': 'MESA'
                                }
                              ],
                                'parceiro': '3',
                                'usuario': 'BPC5154HH',
	                            'senha': '123456',
	                            'lojista': '905154',
	                            'loja': '5177',
                            }";

            return body;

        }

        public string json9Cheque()
        {
            string body = @"{
                              'dadosLiberacao': {
                                'formaLiberacao': '2',
                                'cpf_cnpj': '01744236089',
                                'beneficiario': 'Youdu Udonta',
                                'banco': '001',
                                'agencia': '0102',
                                'tipocontabancaria': '1',
                                'conta': '00225568'
                              },
                              'formaEnvio': {
                                'correio': 'S',
                                'email': 'N',
                                'loja': 'S',
                                'endereco_email': 'teste@tes.com'
                              },
                              'tipoEfetivacao': {
                                'tipoEfetivacao': '2'
                              },
                              'cheques': [
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '1'
                                },
	                            {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '2'
                                },
	                            {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '3'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '4'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '5'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '6'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '7'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '8'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '9'
                                }
                              ],
                              'dadosAdicionais': [
                                {
                                  'campo': 'A_tipoSistema',
                                  'valor': 'MESA'
                                }
                              ],
                                'parceiro': '3',
                                'usuario': 'BPC5154HH',
	                            'senha': '123456',
	                            'lojista': '905154',
	                            'loja': '5177',
                            }";

            return body;

        }

        public string jsonSeguradora()
        {
            string body = @"{
                              'dadosLiberacao': {
                                'formaLiberacao': '2',
                                'cpf_cnpj': '92127483170',
                                'beneficiario': 'Youdu Udonta',
                                'banco': '001',
                                'agencia': '0102',
                                'tipocontabancaria': '1',
                                'conta': '00225568'
                              },
                              'formaEnvio': {
                                'correio': 'S',
                                'email': null,
                                'loja': 'S',
                                'endereco_email': null
                              },
                              'tipoEfetivacao': {
                                'tipoEfetivacao': '2'
                              },
                              'cheques': [
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '1'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '2'
                                },
                                {
                                  'cmc7': '001262680108501145833000877341',
                                  'parcela': '3'
                                }
                              ],
                              'dadosAdicionais': [
                                {
                                  'campo': '',
                                  'valor': ''
                                }
                              ],
                              'parceiro': '3',
                              'usuario': 'BPC5154HH',
                              'senha': '123456',
                              'lojista': '905154',
                              'loja': '5177',
                              'proposta': '3809757906'
                            }";

            return body;

        }

        public string jsonSemCheque(string p0)
        {
            string body = @"{
                              'dadosLiberacao': {
                                'formaLiberacao': '2',
                                'cpf_cnpj': '92127483170',
                                'beneficiario': 'Youdu Udonta',
                                'banco': '0341',
                                'agencia': '5969',
                                'tipocontabancaria': '1',
                                'conta': '306960'
                              },
                              'formaEnvio': {
                                'correio': 's',
                                'email': 's',
                                'loja': 's',
                                'endereco_email': 'teste@tes.com'
                              },
                              'tipoEfetivacao': {
                                'tipoEfetivacao': '2'
                              },                              
                              'dadosAdicionais': [
                                {
                                  'campo': null,
                                  'valor': null
                                }
                              ],
                              'parceiro': '2',
                              'usuario': 'WPO5019',
                              'senha': '123456',
                              'lojista': '905019',
                              'loja': '5019'
                            }";
            JObject objetoJson = JObject.Parse(body);
            objetoJson["proposta"] = p0;
            string json = JsonConvert.SerializeObject(objetoJson);
            return json;

        }

        public string jsonCarne()
        {
            string body = @"{
                              'dadosLiberacao': {
                                'formaLiberacao': '2',
                                'cpf_cnpj': '01744236089',
                                'beneficiario': 'Youdu Udonta',
                                'banco': '001',
                                'agencia': '0102',
                                'tipocontabancaria': '1',
                                'conta': '00225568'
                              },
                              'formaEnvio': {
                                'correio': 'S',
                                'email': 'N',
                                'loja': 'S',
                                'endereco_email': 'teste@tes.com'
                              },
                              'tipoEfetivacao': {
                                'tipoEfetivacao': '2'
                              },                              
                              'dadosAdicionais': [
                                {
                                  'campo': 'A_tipoSistema',
                                  'valor': 'MESA'
                                }
                              ],
                                'parceiro': '3',
                                'usuario': 'BPC5154HH',
                                'senha': '123456',
                                'lojista': '905154',
                                'loja': '5177',
                            }";

            return body;

        }

        public string jsonEfetivacao()
        {
            string body = @"{
                              'uploadConfirmado': 'S',
	                          'dadosAdicionais': [{
                                'Campo': '',
		                        'Valor': ''}],
                              'parceiro': '25',
                              'usuario': 'DACASA1',
                              'senha': '123456',
                              'lojista': '905161',
                              'loja': '5169',
                              'proposta': '3810403985'
                            }";
            return body;
        }
    }
}
