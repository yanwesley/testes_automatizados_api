﻿using BDD.Util.Banco;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace BDD.Util.Massa_Dados
{

    public class GeraProposta
    {
        public Dictionary<string,dynamic> propostasCredito { get; set; } = new Dictionary<string, dynamic>();
        public Dictionary<string, dynamic> propostasOperacoes { get; set; } = new Dictionary<string, dynamic>();
        public Dictionary<string, dynamic> clientes { get; set; } = new Dictionary<string, dynamic>();
        public List<Dictionary<string, dynamic>> h2HCamposAdicionaisProposta { get; set; } = new List<Dictionary<string, dynamic>>();
        public Dictionary<string, dynamic> h2HOfertas { get; set; } = new Dictionary<string, dynamic>();
        public Dictionary<string, dynamic> complementos { get; set; } = new Dictionary<string, dynamic>();

        public string _proposta { get; set; }
        public string _oferta { get; set; }
        public string _ { get; set; }
        public string _cliente { get; set; }
        public string _cpfcnpj { get; set; }
        public DateTime _dataMovimento { get; set; }
        public DateTime _dataPrimeiroVencimento { get; set; }
        public DateTime _dataAtual { get; set; }
        public string _plano { get; set; }
        public string _produto { get; set; }
        public Dictionary<string, dynamic> _simulacao { get; set; }
        private Consulta_SP _con { get; set; }
        public GeraProposta(DateTime dataMovimento)
        {
            _dataMovimento = dataMovimento;
            _dataPrimeiroVencimento = dataMovimento.AddDays(60);
            _dataAtual = DateTime.Now;
            _con = new Consulta_SP();
            geraCpfCliente();
            preparaValores();
            preparaPropostasCredito();            
            preparaPropostasOperacoes();
            preparaClientes();
            preparaH2HCamposAdicionaisProposta();
            preparaOfertas();
            preparaComplementos();
            complementaCampos();
            complementaCamposNovaOferta();
        }
        public GeraProposta()
        {
            _con = new Consulta_SP();

            var res = _con.GetUSP("USP_BUSCAPARAMETROS",
                new Dictionary<string, object>
                {
                    { "@EMPRESA", "01" },

                }
            );

            if (!String.IsNullOrEmpty(res))
            {
                var obj = JProperty.Parse(res);
                _dataMovimento = DateTime.Parse(obj[0]["DataMovimento"].ToString());
            }

            _dataPrimeiroVencimento = _dataMovimento.AddDays(62);
            _dataAtual = DateTime.Now;
            geraCpfCliente();
            preparaValores();
            preparaPropostasCredito();            
            preparaPropostasOperacoes();
            preparaClientes();
            preparaH2HCamposAdicionaisProposta();
            preparaOfertas();
            preparaComplementos();
            complementaCampos();
            complementaCamposNovaOferta();

        }
        public bool insereProposta()
        {
            using (var _t = _con.GetTransaction())
            {
                try
                {
                    insereRegistros();
                    _t.Commit();
                    complementaPosInsert();
                }
                catch (Exception e)
                {
                    _t.Rollback();
                    throw;
                }
            } 
            return true;
        }
        private void geraCpfCliente()
        {
            int soma = 0, resto = 0;
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            Random rnd = new Random();
            string cpf = rnd.Next(100000000, 999999999).ToString();
            for (int i = 0; i < 9; i++)
                soma += int.Parse(cpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            cpf = cpf + resto;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(cpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            cpf = cpf + resto;
            _cliente = cpf+"1";
            _cpfcnpj = cpf;
        }

        private void insereRegistros()
        {
            string res = _con.GetUSP("USP_InserePropostaPronta", 
                new Dictionary<string, object>
                {
                    { "@clientes", JsonConvert.SerializeObject(clientes, Formatting.None)},
                    { "@complementos", JsonConvert.SerializeObject(complementos, Formatting.None)},
                    { "@propostasCredito", JsonConvert.SerializeObject(propostasCredito, Formatting.None)},
                    { "@propostasOperacoes", JsonConvert.SerializeObject(propostasOperacoes, Formatting.None)},
                    { "@h2HOfertas", JsonConvert.SerializeObject(h2HOfertas, Formatting.None)},
                    { "@h2HCamposAdicionaisProposta", JsonConvert.SerializeObject(h2HCamposAdicionaisProposta, Formatting.None)}
                }
                );

            if (!String.IsNullOrEmpty(res))
            {
                var obj = JProperty.Parse(res);
                _proposta = obj[0]["PROPOSTA"].ToString();
                _oferta = obj[0]["ID_Oferta"].ToString();
            }
            else
            {
                throw new Exception("Erro ao carregar numero da proposta");
            }
        }
        private void complementaCampos()
        {
            propostasCredito["CLIENTE"] = _cliente;
            propostasOperacoes["FINDTEMISSAO"] = _dataMovimento;
            clientes["CLIENTE"] = _cliente;
            clientes["CICCGC"] = _cpfcnpj;
            clientes["DATAINCLUSAO"] = _dataAtual.ToString("yyyy-MM-dd");
            clientes["DATARENDAAPROVADA"] = _dataMovimento;
            h2HOfertas["EMISSAO"] = _dataMovimento;
            h2HOfertas["DATAOFERTA"] = _dataMovimento;
            h2HOfertas["DATAMOVIMENTO"] = _dataMovimento;
            complementos["CLIENTE"] = _cliente;
            foreach (var item in h2HCamposAdicionaisProposta)
            {
                item["CLIENTE"] = _cliente;
            }
        }
        private void complementaCamposNovaOferta()
        {
            propostasCredito["PRAZOAPROVADO"] = _simulacao["prazo"];
            propostasCredito["PMTAPROVADO"] = _simulacao["valorPMT"];
         
            propostasOperacoes["FINDTPRIMVENCIM"] = _dataPrimeiroVencimento;
            propostasOperacoes["PRIMVENCIMVRG"] = _dataPrimeiroVencimento;
            propostasOperacoes["PRAZO"] = _simulacao["prazo"].ToString().PadLeft(3, '0');
            propostasOperacoes["VALORCOMPRA"] = _simulacao["valorCompra"];
            propostasOperacoes["FINPRINCIPAL"] = _simulacao["valorCompra"];
            propostasOperacoes["ValorLiberacao"] = _simulacao["valorliberado"];
            propostasOperacoes["FINTAXA"] = _simulacao["taxaPlano"];
            propostasOperacoes["FinIof"] = _simulacao["iof"];
            propostasOperacoes["Iof"] = _simulacao["iof"];
            propostasOperacoes["FinSeguro"] = _simulacao["valorSeguro"];
            propostasOperacoes["ValorInclusaoCadastro"] = _simulacao["valorTC"];
            propostasOperacoes["VLRTOTALPROPOSTA"] = _simulacao["valorTotal"];
            propostasOperacoes["IDPRODUTOSEGURO"] = _simulacao["idProdutoSeguro"];
            propostasOperacoes["TIPO_SEGURO_H2H"] = _simulacao["seguro"];
            propostasOperacoes["ValorRenovacaoCadastro"] = _simulacao["valorTaxaRenovacao"];
            propostasOperacoes["seguradora"] = _simulacao["seguradora"];
           


            h2HOfertas["DATAPRIMEIROVENCIMENTO"] = _dataPrimeiroVencimento;
            h2HOfertas["DATAULTIMOVENCIMENTO"] = _simulacao["dataUltimoVencimento"];
            h2HOfertas["PRAZO"] = _simulacao["prazo"].ToString().PadLeft(3, '0');
            h2HOfertas["CETANUAL"] = _simulacao["taxaCetAnual"];
            h2HOfertas["CETMENSAL"] = _simulacao["taxaCetMensal"];
            h2HOfertas["VALORCREDITO"] = _simulacao["valorCompra"];
            h2HOfertas["VALORLIBERADO"] = _simulacao["valorliberado"];
            h2HOfertas["VALORPARCELA"] = _simulacao["valorPMT"];
            h2HOfertas["TAXA"] = _simulacao["taxaPlano"];
            h2HOfertas["IOF"] = _simulacao["iof"];
            h2HOfertas["VALORSEGURO"] = _simulacao["valorSeguro"];
            h2HOfertas["VALORTAXAINCLUSAO"] = _simulacao["valorTC"];
            h2HOfertas["VALORTOTAL"] = _simulacao["valorTotal"];
            h2HOfertas["VALORFINANCIADO"] = _simulacao["valorFinanciado"];
            h2HOfertas["FINANCIADOINDICE"] = _simulacao["financiadoIndice"];
            h2HOfertas["TAXAANUAL"] = _simulacao["taxaAnual"];
            h2HOfertas["IDPRODUTOSEGURO"] = _simulacao["idProdutoSeguro"];
            h2HOfertas["SEGURO"] = _simulacao["seguro"];
            h2HOfertas["VALORTAXARENOVACAO"] = _simulacao["valorTaxaRenovacao"];
            h2HOfertas["FINIOFSEGURO"] = _simulacao["finIofSeguro"];
            h2HOfertas["seguradora"] = _simulacao["seguradora"];


        }
        private void complementaPosInsert()
        {
            propostasCredito["PROPOSTA"] = _proposta;
            propostasCredito["Id_Oferta"] = _oferta;
            propostasOperacoes["PROPOSTA"] = _proposta;
            foreach (var item in h2HCamposAdicionaisProposta)
            {
                item["PROPOSTA"] = _proposta;
                item["CLIENTE"] = _cliente;
            }
            h2HOfertas["PROPOSTA"] = _proposta;
            h2HOfertas["Id_Oferta"] = _oferta;
        }


        public async Task novosValores(int prazo, decimal valorCompra, string seguro, string primeiroVencimento, string plano, string produto, decimal valorRefin = 0)
        {
            RestClient client = new RestClient("https://uoou0ez904.execute-api.us-east-1.amazonaws.com/DEVELOP/api/calculadora/v1/simulacao");
            var request = new RestRequest(Method.POST);
            var requestBody = new Dictionary<string,dynamic>
            {
                {"Cpf",_cpfcnpj },
                { "DataEmissao", propostasOperacoes["FINDTEMISSAO"].ToString("dd/MM/yyyy") },
                { "DataPrimeiroVencimento", primeiroVencimento },
                { "Loja" , propostasCredito["LOJA"] },
                { "Lojista" , propostasCredito["LOJISTA"] },
                { "Plano" , plano },
                { "PrazoMaximo" , prazo },
                { "PrazoMinimo" , prazo },
                { "Produto" , produto },
                { "Seguro" , (seguro == "2" ? "COMPLETO" : seguro == "1" ? "BASICO" : "SEM_SEGURO" )},
                { "ValorCompra" , valorCompra },
                { "ValorPMT" , null },
                { "ValorRefin" , valorRefin }
            };
            request.AddJsonBody(requestBody);
            IRestResponse response = await client.ExecuteTaskAsync(request);
            var r = JArray.Parse(response.Content);
            if (r[0]["code"]?.ToString() != null && r[0]["code"]?.ToString() != "200")
            {
                throw new Exception("Erro ao gerar motor de ofertas: "+response.Content);
            }
            // Arruma os valores alterados para fazer o insert
            _dataPrimeiroVencimento = Convert.ToDateTime(primeiroVencimento); 
            _plano = plano;
            _produto = produto;
            h2HOfertas["PRODUTO"] = _produto;
            h2HOfertas["PLANO"] = _plano;
            propostasCredito["PLANO"] = _plano;
            propostasCredito["PRODUTO"] = _produto;
            propostasCredito["PLANO"] = _plano;
            propostasCredito["PRODUTO"] = _produto;
            propostasOperacoes["PRODUTO"] = _produto;
            propostasOperacoes["PLANO"] = _plano;

            _simulacao["prazo"] = r[0]["prazo"].ToString();
            _simulacao["taxaCetMensal"] = decimal.Parse(r[0]["taxaCetMensal"].ToString());
            _simulacao["taxaCetAnual"] = decimal.Parse(r[0]["taxaCetAnual"].ToString());
            _simulacao["valorCompra"] = decimal.Parse(r[0]["valorCompra"].ToString());
            _simulacao["valorPMT"] = decimal.Parse(r[0]["valorPMT"].ToString());
            _simulacao["taxaPlano"] = decimal.Parse(r[0]["taxaPlano"].ToString());
            _simulacao["iof"] = decimal.Parse(r[0]["iof"].ToString());
            _simulacao["valorSeguro"] = decimal.Parse(r[0]["valorSeguro"].ToString());
            _simulacao["valorTC"] = decimal.Parse(r[0]["valorTC"].ToString());
            _simulacao["valorTotal"] = decimal.Parse(r[0]["valorTotal"].ToString());
            _simulacao["valorFinanciado"] = decimal.Parse(r[0]["valorFinanciado"].ToString());
            _simulacao["tipoDeCalculo"] = r[0]["tipoDeCalculo"].ToString();
            _simulacao["mensagemAdicional"] = r[0]["mensagemAdicional"]?.ToString();
            _simulacao["dataUltimoVencimento"] = r[0]["dataUltimoVencimento"].ToString();
            _simulacao["dataMovimento"] = r[0]["dataMovimento"].ToString();
            _simulacao["descPlano"] = r[0]["descPlano"].ToString();
            _simulacao["descProduto"] = r[0]["descProduto"].ToString();
            _simulacao["financiadoIndice"] = decimal.Parse(r[0]["financiadoIndice"].ToString());
            _simulacao["finIofSeguro"] = decimal.Parse(r[0]["finIofSeguro"].ToString());
            _simulacao["idProdutoSeguro"] = r[0]["idProdutoSeguro"].ToString();
            _simulacao["seguro"] = r[0]["seguro"].ToString();
            _simulacao["taxaAnual"] = decimal.Parse(r[0]["taxaAnual"].ToString());
            _simulacao["valorliberado"] = decimal.Parse(r[0]["valorliberado"].ToString());
            _simulacao["valorTaxaRenovacao"] = decimal.Parse(r[0]["valorTaxaRenovacao"].ToString());
            _simulacao["valorTotalPst"] = decimal.Parse(r[0]["valorTotalPst"].ToString());
            _simulacao["valorRefin"] = decimal.Parse(r[0]["valorRefin"].ToString());
            _simulacao["seguradora"] = r[0]["seguradora"]?.ToString();

            if (_simulacao["valorSeguro"] == 0)
            {
                _simulacao["seguro"] = 0;
            }
            complementaCamposNovaOferta();

        } 

        public virtual void preparaValores() { }
        public virtual void preparaPropostasCredito() { }
        public virtual void preparaPropostasOperacoes() { }
        public virtual void preparaClientes() { }
        public virtual void preparaH2HCamposAdicionaisProposta() { }
        public virtual void preparaOfertas() { }
        public virtual void preparaComplementos() { }
    }

    public class PropostaDaCasa : GeraProposta
    {
        public PropostaDaCasa(DateTime dataMovimento) : base(dataMovimento) { }
        public PropostaDaCasa() : base() { }
        public override void preparaValores()
        {
            _simulacao = new Dictionary<string, dynamic>
            {
                {"prazo", 15},
                {"taxaCetMensal", 11.8},
                {"taxaCetAnual", 288.31},
                {"valorCompra", 20000.0},
                {"valorPMT", 3221.11},
                {"taxaPlano", 11.5},
                {"iof", 542.88},
                {"valorSeguro", 0.0},
                {"valorTC", 39.9},
                {"valorTotal", 48316.65},
                {"valorFinanciado", 20582.78},
                {"tipoDeCalculo", "CREDITO"},
                {"mensagemAdicional", "Seguro recusado."},
                {"dataUltimoVencimento", "01/05/2021"},
                {"dataMovimento", "06/01/2020"},
                {"descPlano", "3840-DACASA TAXA 11 50"},
                {"descProduto", "001049-CREDITO PESSOAL CARNE DACASA"},
                {"financiadoIndice", 20039.9},
                {"finIofSeguro", 0.0},
                {"idProdutoSeguro", 0},
                {"seguro", 0},
                {"taxaAnual", 269.2312},
                {"valorliberado", 20000.0},
                {"valorTaxaRenovacao", 0.0},
                {"valorTotalPst", 0.0},
                {"valorRefin", 0.0},
                {"seguradora", null }
            };
        }
        public override void preparaPropostasCredito()
        {
            propostasCredito = new Dictionary<string, dynamic>
                {
                    {"EMPRESA" , "01"},
                    {"LOJA" , "5169"},
                    {"LOJISTA" , "905161"},
                    {"CANAL" , "00"},
                    {"VENDEDOR" , "01 "},
                    {"AGENCIA" , "0001"},
                    {"PRODUTO" , "001049"},
                    {"TIPOPROPOSTA" , "P"},
                    {"DATAINCLUSAO" , _dataMovimento},
                    {"HORAINICIO" , "16:37   "},
                    {"SITUACAO" , "21"},
                    {"OBSERVACAO" , "OK"},
                    {"PARECER" , "**APROVADA AUTOMATICAMENTE"},
                    {"APROVADOR" , "APROVADOR "},
                    {"DATAAPROVACAO" , _dataPrimeiroVencimento},
                    {"HORAAPROVACAO" , "16:43:40"},
                    {"DIGITADOR" , "DACASA1"},
                    {"HORAINCLUSAO" , "16:37   "},
                    {"PAGAMENTO" , "N"},
                    {"ORIGEM" , "H"},
                    {"MIDIA" , "88"},
                    {"TIPOAPROVACAO" , "0"},
                    {"PRINCIPALAPROVADO" , "0.00"},
                    {"USUARIO" , "DACASA1   "},
                    {"ALTERACAOCADASTRAL" , "S"},
                    {"CodigoNeuroTech" , "513181610430523363"},
                    {"CRMPreAprovado" , "3"},
                    {"UltSituacao" , ""},
                    {"CODMALADIRETA" , ""},
                    {"CodAnalisadorAutomatico" , "4"},
                    {"ETAPA" , "4"},
                    {"IDCRMPREAPROVADOS" , "0"},
                    {"Excecao" , "N"},
                    {"SituacaoAnterior" , "21"},
                    {"PARCEIRO_ID" , "25"},
                };
        }
        public override void preparaPropostasOperacoes()
        {
            propostasOperacoes = new Dictionary<string, dynamic>
                {
                    {"EMPRESA" , "01"},
                    {"AGENCIA" , "0001"},
                    {"PRODUTO" , "001049"},
                    {"LOJISTA" , "905161"},
                    {"LOJA" , "5169  "},
                    {"PLANO" , "3840"},
                    {"MOEDA" , "01"},
                    {"FINTAC" , "0.00"},
                    {"AVALISTA1" , "               "},
                    {"AVALISTA2" , "               "},
                    {"CONVENIADA" , "      "},
                    {"TIPOOPERACAO" , "C"},
                    {"VALORVRGFINAL" , "0.00"},
                    {"PRESTACAO" , "907.73"},
                    {"AGENTE" , "    "},
                    {"PLANOFLEXIVEL" , "N"},
                    {"TAXAFLEXIVEL" , "0.000000"},
                    {"TACFLEXIVEL" , "0.00"},
                    {"TACFINANCIADA" , "S"},
                    {"IOCFINANCIADO" , "S"},
                    {"notafiscal" , "          "},
                    {"Matricula" , ""},
                    {"VLRPRIMPRESTACAO" , "907.73"},
                    {"ValorTotalPST" , "0.00"},
                    {"CodAutenticidade" , "          "},
                    {"FINIOFSEGURO" , "1.24"},
                    {"TIPO_CONTRATO" , "0"}
                };
        }
        public override void preparaClientes()
        {
            clientes = new Dictionary<string, dynamic>{
                    {"EMPRESA" , "01"},
                    {"TIPOPESSOA" , "F"},
                    {"NOME" , "MARLY FIGUEIREDO DOS SANTOS"},
                    {"NASCIMENTO" , "1961-05-15 00:00:00.000"},
                    {"SEXO" , "F"},
                    {"NATURALIDADE" , "FIRMINO ALVES"},
                    {"MAE" , "JOVELINA CRISTINA DE JESUS"},
                    {"COSIF" , "4600"},
                    {"NATURALIDADEUF" , "  "},
                    {"CONVENIADA" , "      "},
                    {"CONVENIADAORGAO" , "     "},
                    {"CODIGOEXTERNO" , "14628030"},
                    {"Matricula" , ""},
                    {"CNHCliente" , ""},
                    {"RENDAAPROVADA" , "15500.00"},
                    {"CODIGOSEGURANCACNH" , "            "},
                    {"USUARIO_SICRED_ALTERACAO" , "DACASA1"}

                };
        }
        public override void preparaH2HCamposAdicionaisProposta()
        {
            h2HCamposAdicionaisProposta = new List<Dictionary<string, dynamic>>{
                new Dictionary<string, dynamic>
                {
                    //{"ID", "41450684"}, // automatico 
                    {"EMPRESA", "01"},
                    {"NOMECAMPOWS_ID", "346"},
                    {"VALOR", "APROVADO"}
                }
            };
        }
        public override void preparaOfertas()
        {
            h2HOfertas = new Dictionary<string, dynamic>
                {
                    {"PRODUTO" , "001049"},
                    {"PLANO" , "3840"},
                    {"TAC" , "0.000000"},
                    {"HORAOFERTA" , "04:02:24"},
                    {"DESCPLANO" , "3840-DACASA TAXA 11 50"},
                    {"DESCPRODUTO" , "3840-DACASA TAXA 11 50"},
                    {"VALORTOTALPST" , "0.00"},
                    {"TIPOSIMULACAO" , "PrestacaoAproximada "},
                    {"ORIGEMOFERTA" , "1         "},
                    {"ATIVO" , "S"}

                };
        }
        public override void preparaComplementos()
        {
            complementos = new Dictionary<string, dynamic>
                {
                    {"EMPRESA" , "01"},
                    {"NACIONALIDADE" , "01"},
                    {"UFNASCIMENTO" , "BA"},
                    {"ESTCIVIL" , "C"},
                    {"DEPENDENTES" , "0"},
                    {"PAI" , ""},
                    {"CONJUGE" , ""},
                    {"GRAUINSTRUCAO" , "0"},
                    {"NRODOCIDENT" , "0446244503    "},
                    {"UFEMIDOCIDENT" , "BA"},
                    {"ORGAOEMISSOR" , "38"},
                    {"DATAEMIDOCIDENT" , "2003-01-24 00:00:00.000"},
                    {"RESENDERECO" , "RUA ALICE AGE 03 "},
                    {"RESBAIRRO" , "NONA ITAPETINGA"},
                    {"RESCEP" , "45700000"},
                    {"RESCIDADE" , "ITAPETINGA"},
                    {"RESUF" , "BA"},
                    {"RESTIPORESIDENCIA" , "1"},
                    {"RESDDD" , "77  "},
                    {"RESFONE" , "988422115  "},
                    {"RESTIPOFONE1" , "2"},
                    {"RESDDD2" , "    "},
                    {"RESFONE2" , "           "},
                    {"RESTIPOFONE2" , " "},
                    {"INFOEMAIL" , ""},
                    {"CORRESPONDENCIA" , "2"},
                    {"COMBAIRRO" , ""},
                    {"COMCEP" , "45700000"},
                    {"COMCIDADE" , ""},
                    {"COMUF" , "  "},
                    {"COMDDD" , "0"},
                    {"COMFONE" , "000000000"},
                    {"COMRAMAL" , ""},
                    {"ATIVEMPRESA" , "INSS"},
                    {"ATIVSETOR" , ""},
                    {"ATIVRENDA" , "15500.00"},
                    {"ATIVANOS" , "11"},
                    {"ATIVMESES" , "3 "},
                    {"ATIVOUTRASVALOR" , "0.00"},
                    {"OUTRASRENDASDESCRICAO" , ""},
                    {"REFBANCO1" , "    "},
                    {"REFTIPOCONTA1" , "  "},
                    {"REFAGENCIA1" , ""},
                    {"REFCIDADE1" , ""},
                    {"REFUF1" , "  "},
                    {"REFBANCO2" , "    "},
                    {"REFTIPOCONTA2" , "  "},
                    {"REFAGENCIA2" , ""},
                    {"REFCIDADE2" , ""},
                    {"REFUF2" , "  "},
                    {"REFPESSNOME1" , "MARCIA CRISTINA MACHADO"},
                    {"REFPESSREL1" , ""},
                    {"REFPESSDDD1" , "0"},
                    {"REFPESSFONE1" , "000000000"},
                    {"REFPESSNOME2" , "NICOLE CRISTINA"},
                    {"REFPESSREL2" , ""},
                    {"REFPESSDDD2" , "00"},
                    {"REFPESSFONE2" , "000000000"},
                    {"RESTEMPOMORADIA" , "44"},
                    {"CONJUGECPF" , "           "},
                    {"CONJUGEEMPRESA" , ""},
                    {"CONJUGEENDERECO" , ""},
                    {"CONJUGEBAIRRO" , ""},
                    {"CONJUGECIDADE" , ""},
                    {"CONJUGEUF" , "  "},
                    {"CONJUGECEP" , "        "},
                    {"CONJUGEDDD" , ""},
                    {"CONJUGEFONE" , ""},
                    {"CONJUGERAMAL" , "     "},
                    {"CONJUGEATIVIDADE" , "    "},
                    {"CONJUGECARGO" , "               "},
                    {"CONJUGERENDA" , "0.00"},
                    {"CONJUGEANOS" , "0"},
                    {"CONJUGEMESES" , "0"},
                    {"EMPRESACGC" , "              "},
                    {"CONJUGENASCIMENTO" , "1753-01-01 12:00:00.000"},
                    {"ATIVDATAADMISSAO" , "2009-01-01 00:00:00.000"},
                    {"REFTEMPOCONTA1" , "      "},
                    {"REFTEMPOCONTA2" , "      "},
                    {"CONJUGEADMISSAO" , "1753-01-01 12:00:00.000"},
                    {"RESMESANO" , "011976"},
                    {"REFPESSRAMAL1" , "    "},
                    {"REFPESSRAMAL2" , "    "},
                    {"REFCONTACORRENTE1" , ""},
                    {"TIPODOCIDENT" , "01"},
                    {"REFCONTACORRENTE2" , ""},
                    {"ClasseProfissional" , "02"},
                    {"Atividade" , "    "},
                    {"OESERASA" , "SPTC "},
                    {"ResTipoFone3" , " "},
                    {"ResDDD3" , "    "},
                    {"ResFone3" , "           "},
                    {"REFBANCO3" , "    "},
                    {"REFTIPOCONTA3" , "      "},
                    {"REFAGENCIA3" , ""},
                    {"REFCIDADE3" , ""},
                    {"REFUF3" , "  "},
                    {"REFCONTACORRENTE3" , ""},
                    {"REFTEMPOCONTA3" , "      "},
                    {"AtivCNPJ" , ""},
                    {"RefCartaoEmissor1" , "0"},
                    {"RefCartaoEmissor2" , "0"},
                    {"AtivOutrasDDD" , ""},
                    {"AtivOutrasTelefone" , ""},
                    {"AtivOutrasCNPJ" , ""},
                    {"AtivOutrasNumBeneficio" , ""},
                    {"AtivOutrasEspecieBeneficio" , ""},
                    {"AtivNumBeneficio" , ""},
                    {"AtivEspecieBeneficio" , ""},
                    {"RefLimiteCredito1" , "0.00"},
                    {"RefLimiteCredito2" , "0.00"},
                    {"REFLIMITECHEQUE1" , "0.00"},
                    {"REFLIMITECHEQUE2" , "0.00"},
                    {"REFLIMITECHEQUE3" , "0.00"},
                    {"ResLogradouro" , "RUA ALICE AGE"},
                    {"ResNumero" , "03"},
                    {"ResComplemento" , ""},
                    {"ComEndLogradouro" , ""},
                    {"ComEndNumero" , ""},
                    {"ComEndComplemento" , ""},
                    {"AtivOutrasLogradouro" , ""},
                    {"AtivOutrasNumero" , ""},
                    {"AtivOutrasComplemento" , ""},
                    {"AtivOutrasBairro" , ""},
                    {"AtivOutrasCidade" , ""},
                    {"AtivOutrasCEP" , "        "},
                    {"AtivOutrasUF" , "  "},
                    {"RefBancTipoConta1" , "0"},
                    {"RefBancTipoConta2" , "0"},
                    {"RefBancTipoConta3" , "0"},
                    {"CONJUGENUMERO" , ""},
                    {"CONJUGECOMPL" , ""},
                    {"NEGATIVADO" , " "},
                    {"POSSUICHEQUE" , " "},
                    {"USUARIO_SICRED_ALTERACAO" , "DACASA1"}
                };
        }
    }
    public class PropostaLosango : GeraProposta
    {
        public PropostaLosango(DateTime dataMovimento) : base(dataMovimento) { }
        public PropostaLosango() : base() { }
        public override void preparaValores()
        {
            _simulacao = new Dictionary<string, dynamic>
            {
                {"prazo", 09},
                {"taxaCetMensal", 17.43},
                {"taxaCetAnual", 587.90},
                {"valorCompra", 3000.00},
                {"valorPMT", 806.87},
                {"taxaPlano", 11.5},
                {"iof", 56.760000},
                {"valorSeguro", 0.0},
                {"valorTC", 0.000000},
                {"valorTotal", 7261.83},
                {"valorFinanciado", 3000.00},
                {"tipoDeCalculo", "CREDITO"},
                {"mensagemAdicional", "Seguro recusado."},
                {"dataUltimoVencimento", "01/05/2021"},
                {"dataMovimento", "06/01/2020"},
                {"descPlano", "3840-DACASA TAXA 11 50"},
                {"descProduto", "001049-CREDITO PESSOAL CARNE DACASA"},
                {"financiadoIndice", 3000.000000},
                {"finIofSeguro", 0.0},
                {"idProdutoSeguro", 0},
                {"seguro", 0},
                {"taxaAnual", 543.940000},
                {"valorliberado", 3000.00},
                {"valorTaxaRenovacao", 0.0},
                {"valorTotalPst", 0.0},
                {"valorRefin", 0.0},
                {"seguradora", null }
            };
        }
        public override void preparaPropostasCredito()
        {
            propostasCredito = new Dictionary<string, dynamic>
                {
                    {"EMPRESA", "01"},
                    {"LOJA", "5203"},
                    {"LOJISTA", "905166"},
                    {"CANAL", "00"},
                    {"VENDEDOR", "01 "},
                    {"AGENCIA", "0001"},
                    {"PRODUTO", "001015"},
                    {"TIPOPROPOSTA", "P"},
                    {"DATAINCLUSAO", "2020-01-06 00:00:00.000"},
                    {"HORAINICIO", "11:20   "},
                    {"SITUACAO", "21"},
                    {"OBSERVACAO", "OK"},
                    {"PARECER", "**APROVADA AUTOMATICAMENTE"},
                    {"APROVADOR", "APROVADOR "},
                    {"DATAAPROVACAO", "2020-02-28 00:00:00.000"},
                    {"HORAAPROVACAO", "11:22:23"},
                    {"DIGITADOR", "LOSANGO1"},
                    {"HORAINCLUSAO", "11:20   "},
                    {"PAGAMENTO", "N"},
                    {"ORIGEM", "H"},
                    {"MIDIA", "88"},
                    {"TIPOAPROVACAO", "0"},
                    {"PRINCIPALAPROVADO", "0.00"},
                    {"USUARIO", "LOSANGO1  "},
                    {"ALTERACAOCADASTRAL", "S"},
                    {"CodigoNeuroTech", "513181610430523940"},
                    {"CRMPreAprovado", "3"},
                    {"UltSituacao", "  "},
                    {"CODMALADIRETA", ""},
                    {"CodAnalisadorAutomatico", "4"},
                    {"ETAPA", "6"},
                    {"IDCRMPREAPROVADOS", "0"},
                    {"Excecao", "N"},
                    {"SituacaoAnterior", "05"},
                    {"PARCEIRO_ID", "1"}
                };
        }
        public override void preparaPropostasOperacoes()
        {
            propostasOperacoes = new Dictionary<string, dynamic>
                {
                    {"EMPRESA", "01"},
                    {"AGENCIA", "0001"},
                    {"PRODUTO", "001015"},
                    {"LOJISTA", "905166"},
                    {"LOJA", "5203  "},
                    {"PLANO", "3503"},
                    {"MOEDA", "01"},
                    {"FINTAC", "0.00"},
                    {"AVALISTA1", "               "},
                    {"AVALISTA2", "               "},
                    {"CONVENIADA", "      "},
                    {"TIPOOPERACAO", "C"},
                    {"VALORVRGFINAL", "0.00"},
                    {"PRESTACAO", "806.87"},
                    {"AGENTE", "    "},
                    {"PLANOFLEXIVEL", "N"},
                    {"TAXAFLEXIVEL", "0.000000"},
                    {"TACFLEXIVEL", "0.00"},
                    {"TACFINANCIADA", "S"},
                    {"IOCFINANCIADO", "S"},
                    {"notafiscal", "          "},
                    {"Matricula", ""},
                    {"VLRPRIMPRESTACAO", "806.87"},
                    {"ValorTotalPST", "0.00"},
                    {"CodAutenticidade", "          "},
                    {"FINIOFSEGURO", "0.00"},
                    {"TIPO_CONTRATO", "0"}
                };
        }
        public override void preparaClientes()
        {
            clientes = new Dictionary<string, dynamic>{
                    {"EMPRESA", "01"},
                    {"TIPOPESSOA", "F"},
                    {"NOME", "RAFAEL TEIXEIRA"},
                    {"NASCIMENTO", "1982-02-09 00:00:00.000"},
                    {"SEXO", "M"},
                    {"NATURALIDADE", "VARZEA GRANDE"},
                    {"MAE", "MARIA FRANCISCA DA SILVA"},
                    {"COSIF", "4600"},
                    {"NATURALIDADEUF", "  "},
                    {"CONVENIADA", "      "},
                    {"CONVENIADAORGAO", "     "},
                    {"CODIGOEXTERNO", "14628089"},
                    {"Matricula", ""},
                    {"CNHCliente", ""},
                    {"RENDAAPROVADA", "15500.00"},
                    {"CODIGOSEGURANCACNH", "            "},
                    {"USUARIO_SICRED_ALTERACAO", "LOSANGO1" }
                };
        }
        public override void preparaH2HCamposAdicionaisProposta()
        {
            h2HCamposAdicionaisProposta = new List<Dictionary<string, dynamic>>{
                new Dictionary<string, dynamic>{ {"EMPRESA", "01"},  {"NOMECAMPOWS_ID","148"},  {"VALOR","S"}},
                new Dictionary<string, dynamic>{ {"EMPRESA", "01"},  {"NOMECAMPOWS_ID","149"},  {"VALOR","11408184737"}},
                new Dictionary<string, dynamic>{ {"EMPRESA", "01"},  {"NOMECAMPOWS_ID","148"},  {"VALOR","S"}},
                new Dictionary<string, dynamic>{ {"EMPRESA", "01"},  {"NOMECAMPOWS_ID","149"},  {"VALOR","01095506170"}},
                new Dictionary<string, dynamic>{ {"EMPRESA", "01"},  {"NOMECAMPOWS_ID","148"},  {"VALOR","S"}},
                new Dictionary<string, dynamic>{ {"EMPRESA", "01"},  {"NOMECAMPOWS_ID","149"},  {"VALOR","01095506170"}}
            };
        }
        public override void preparaOfertas()
        {
            h2HOfertas = new Dictionary<string, dynamic>
                {
                    {"PRODUTO", "001015"},
                    {"PLANO", "3503"},
                    {"TAC", "0.000000"},
                    {"HORAOFERTA", "11:02:29"},
                    {"DESCPLANO", "EPCHEQUE LOSANGO REC300 1599 8 A 36"},
                    {"DESCPRODUTO", "CRÉDITO PESSOAL CHEQUE LOSANGO"},
                    {"VALORTOTALPST", "0.00"},
                    {"TIPOSIMULACAO", "ValorCredito        "},
                    {"ORIGEMOFERTA", "CLIENTE   "},
                    {"ATIVO", "S"}
                };
        }
        public override void preparaComplementos()
        {
            complementos = new Dictionary<string, dynamic>
                {
                    {"EMPRESA", "01"},
                    {"NACIONALIDADE", "01"},
                    {"UFNASCIMENTO", "MT"},
                    {"ESTCIVIL", "C"},
                    {"DEPENDENTES", "0"},
                    {"PAI", ""},
                    {"CONJUGE", ""},
                    {"GRAUINSTRUCAO", "5"},
                    {"NRODOCIDENT", "02680360      "},
                    {"UFEMIDOCIDENT", "MT"},
                    {"ORGAOEMISSOR", "01"},
                    {"DATAEMIDOCIDENT", "2009-01-13 00:00:00.000"},
                    {"RESENDERECO", "R S JUDAS TADEU 5 "},
                    {"RESBAIRRO", "PTE NOVA"},
                    {"RESCEP", "90820200"},
                    {"RESCIDADE", "VARZEA GRANDE"},
                    {"RESUF", "MT"},
                    {"RESTIPORESIDENCIA", "1"},
                    {"RESDDD", "65  "},
                    {"RESFONE", "999400124  "},
                    {"RESTIPOFONE1", "1"},
                    {"RESDDD2", "    "},
                    {"RESFONE2", "           "},
                    {"RESTIPOFONE2", " "},
                    {"INFOEMAIL", ""},
                    {"CORRESPONDENCIA", "2"},
                    {"COMBAIRRO", "SANTO ANTONIO "},
                    {"COMCEP", "78118000"},
                    {"COMCIDADE", "CUIABA"},
                    {"COMUF", "MT"},
                    {"COMDDD", "65"},
                    {"COMFONE", "999400124"},
                    {"COMRAMAL", ""},
                    {"ATIVEMPRESA", "PENSIONISTA "},
                    {"ATIVSETOR", "1179"},
                    {"ATIVRENDA", "15500.00"},
                    {"ATIVANOS", "11"},
                    {"ATIVMESES", "3 "},
                    {"ATIVOUTRASVALOR", "0.00"},
                    {"OUTRASRENDASDESCRICAO", ""},
                    {"REFBANCO1", "0237"},
                    {"REFTIPOCONTA1", "1 "},
                    {"REFAGENCIA1", "0417"},
                    {"REFCIDADE1", "CUIABA"},
                    {"REFUF1", "MT"},
                    {"REFBANCO2", "    "},
                    {"REFTIPOCONTA2", "  "},
                    {"REFAGENCIA2", ""},
                    {"REFCIDADE2", ""},
                    {"REFUF2", "  "},
                    {"REFPESSNOME1", "JOEVERSON FILHO"},
                    {"REFPESSREL1", "FILHO"},
                    {"REFPESSDDD1", "65"},
                    {"REFPESSFONE1", "99387493"},
                    {"REFPESSNOME2", "REFERENCIA DO CLIENTE"},
                    {"REFPESSREL2", "FILHO"},
                    {"REFPESSDDD2", "065"},
                    {"REFPESSFONE2", "99387490"},
                    {"RESTEMPOMORADIA", "15"},
                    {"CONJUGECPF", "           "},
                    {"CONJUGEEMPRESA", ""},
                    {"CONJUGEENDERECO", ""},
                    {"CONJUGEBAIRRO", ""},
                    {"CONJUGECIDADE", ""},
                    {"CONJUGEUF", "  "},
                    {"CONJUGECEP", "        "},
                    {"CONJUGEDDD", ""},
                    {"CONJUGEFONE", ""},
                    {"CONJUGERAMAL", "     "},
                    {"CONJUGEATIVIDADE", "    "},
                    {"CONJUGECARGO", "0              "},
                    {"CONJUGERENDA", "0.00"},
                    {"CONJUGEANOS", "0"},
                    {"CONJUGEMESES", "0"},
                    {"EMPRESACGC", "              "},
                    {"CONJUGENASCIMENTO", "1753-01-01 12:00:00.000"},
                    {"ATIVDATAADMISSAO", "2009-01-13 00:00:00.000"},
                    {"REFTEMPOCONTA1", "012005"},
                    {"REFTEMPOCONTA2", "      "},
                    {"CONJUGEADMISSAO", "1753-01-01 12:00:00.000"},
                    {"RESMESANO", "012005"},
                    {"REFPESSRAMAL1", "0   "},
                    {"REFPESSRAMAL2", "0   "},
                    {"REFCONTACORRENTE1", "00851973"},
                    {"TIPODOCIDENT", "01"},
                    {"REFCONTACORRENTE2", ""},
                    {"ClasseProfissional", "13"},
                    {"Atividade", "1179"},
                    {"OESERASA", "SSP  "},
                    {"ResTipoFone3", " "},
                    {"ResDDD3", "    "},
                    {"ResFone3", "           "},
                    {"REFBANCO3", "    "},
                    {"REFTIPOCONTA3", "      "},
                    {"REFAGENCIA3", ""},
                    {"REFCIDADE3", ""},
                    {"REFUF3", "  "},
                    {"REFCONTACORRENTE3", ""},
                    {"REFTEMPOCONTA3", "      "},
                    {"AtivCNPJ", ""},
                    {"RefCartaoEmissor1", "0"},
                    {"RefCartaoEmissor2", "0"},
                    {"AtivOutrasDDD", ""},
                    {"AtivOutrasTelefone", ""},
                    {"AtivOutrasCNPJ", ""},
                    {"AtivOutrasNumBeneficio", ""},
                    {"AtivOutrasEspecieBeneficio", ""},
                    {"AtivNumBeneficio", "16150759172"},
                    {"AtivEspecieBeneficio", "05"},
                    {"RefLimiteCredito1", "0.00"},
                    {"RefLimiteCredito2", "0.00"},
                    {"REFLIMITECHEQUE1", "0.00"},
                    {"REFLIMITECHEQUE2", "0.00"},
                    {"REFLIMITECHEQUE3", "0.00"},
                    {"ResLogradouro", "R S JUDAS TADEU"},
                    {"ResNumero", "5"},
                    {"ResComplemento", ""},
                    {"ComEndLogradouro", "RUA CENTRO "},
                    {"ComEndNumero", "5"},
                    {"ComEndComplemento", ""},
                    {"AtivOutrasLogradouro", ""},
                    {"AtivOutrasNumero", ""},
                    {"AtivOutrasComplemento", ""},
                    {"AtivOutrasBairro", ""},
                    {"AtivOutrasCidade", ""},
                    {"AtivOutrasCEP", "        "},
                    {"AtivOutrasUF", "  "},
                    {"RefBancTipoConta1", "1"},
                    {"RefBancTipoConta2", "0"},
                    {"RefBancTipoConta3", "0"},
                    {"CONJUGENUMERO", ""},
                    {"CONJUGECOMPL", ""},
                    {"NEGATIVADO", " "},
                    {"POSSUICHEQUE", " "},
                    {"USUARIO_SICRED_ALTERACAO", "LOSANGO1"}
                };
        }
    }
}
