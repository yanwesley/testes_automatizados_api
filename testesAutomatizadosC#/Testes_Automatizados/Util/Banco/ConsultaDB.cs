﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace BDD.Util.Banco
{
    public class Consulta_SP
    {
        private readonly SqlServerContext conexao;
        private List<dynamic> retorno;
        private string res;
        private IDbTransaction _transaction;
        public Consulta_SP()
        {
            conexao = new SqlServerContext(BuscaConnectionString());
        }

        private IConfiguration BuscaConnectionString()
        {
            IConfiguration configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

            return configuration;
        }

        public IDbTransaction GetTransaction()
        {
            if (_transaction == null)
            {
                _transaction = conexao.BeginTransaction();
            }
            return _transaction;
        }

        public string GetUSP(string usp, Dictionary<string, object> parameters)
        {
            var queryParameter = new DynamicParameters();

            if (parameters != null)
            {
                foreach (var item in parameters)
                    queryParameter.Add(item.Key, item.Value);
            }

            retorno = QueryProcedure<dynamic>(conexao.Connection(), $"dbo.{usp}", queryParameter);
            res = (JsonConvert.SerializeObject(retorno, Formatting.Indented));


            return res;
        }

        public List<T> QueryProcedure<T>(IDbConnection sqlConnection, string procedure, DynamicParameters parameters = null)
        {
            return sqlConnection.Query<T>(procedure, param: parameters, commandType: CommandType.StoredProcedure, transaction: _transaction).ToList();
        }

        public List<T> QueryMultiple<T>(SqlConnection sqlConnection, string procedure, DynamicParameters parameters = null) 
        {
            var res = sqlConnection.QueryMultiple(procedure, param: parameters, commandType: CommandType.StoredProcedure, commandTimeout: 30, transaction: conexao.Transaction);
            return GridRead<T>(res);
        }

        private List<T> GridRead<T>(GridReader result) 
        {
            var resultList = new List<T>();
            using (var gridReader = result)
            {
                while (!gridReader.IsConsumed)
                {
                    var resultSet = result.Read<T>();
                    resultList.AddRange(resultSet.ToList());
                }
            }
            return resultList;
        }
    }
}

