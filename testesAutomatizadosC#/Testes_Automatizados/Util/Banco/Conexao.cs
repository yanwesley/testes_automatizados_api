﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BDD.Util.Banco
{
    public class SqlServerContext
    {
        private readonly IConfiguration _configuration;
        private SqlConnection connection;
        public IDbTransaction Transaction { get; private set; }

        public SqlServerContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IDbTransaction BeginTransaction()
        {
            if (Transaction == null)
                Transaction = Connection().BeginTransaction();

            return Transaction;
        }

        public IDbConnection Connection()
        {
            if (connection == null)
                connection = new SqlConnection(_configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value);

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            return connection;
        }
    }
}
