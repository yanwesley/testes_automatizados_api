﻿using BDD.Util.Banco;
using BDD.Util.Massa_Dados;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Testes_Automatizados.Page
{
    public class GeralPage
    {
        public PreEfetivaPage preEfetiva { get; private set; }
        public EfetivaPage efetiva { get; private set; }
        public GarantiaCDC garantiaCDC { get; private set; }
        public GeraProposta geraProposta { get;set; }
        public string retornoApi { get; private set; }
        public Dictionary<string, string> situacoes = new Dictionary<string, string>
        {
            { "PRONTO PARA CONTRATO","21" },
            { "PRE-EFETIVADO","40" },
        };

        public GeralPage()
        {
            preEfetiva = new PreEfetivaPage();
            efetiva = new EfetivaPage();
            garantiaCDC = new GarantiaCDC();
            //geraProposta = new PropostaLosango();
        }

        private RestRequest restRequest;
        private RestClient restClient;
        public IRestResponse Res;
        public int resp;
        private static ConcurrentDictionary<string, string> returnVariables = new ConcurrentDictionary<string, string>();
        private JObject respMongo;
        public String respDB;
        private DateTime date;
        private DateTime date2;
        private String parceiroMongo, cpfMongo;
        private Consulta_SP consulta;
        public string body, chassi, proposta, situacao, cmc7, parcela, parceiro, loja, lojista, usuario, senha, formaLiberacao, cpf, beneficiario, tipoEfetivacao, valorLiberacao;
        //public GeraProposta proposta;
        private JObject bodyJson;
        //internal dynamic loja;

        public void LimpaVariaveisbanco()
        {
            respDB = null;
        }
                      

        public void LimpaVariaveis()
        {
            returnVariables.Clear();
        }

        public void InicializaJsonBase()
        {
            if (bodyJson == null)
            {
                string jsonBase = body; //new Massa_json().jsonOk();
                bodyJson = JObject.Parse(jsonBase);
            }
        }

        public void AtribuiVariavelJsonBase(string campo, string valor)
        {
            bodyJson[campo] = valor;
        }

        public void FechaJsonBase()
        {
            if (bodyJson != null)
            {
                string json = JsonConvert.SerializeObject(bodyJson);
                body = json;
            }
        }


        public static IConfigurationRoot TesteAPISteps()
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            IConfigurationRoot _config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build();
            return _config;
        }


        public RestClient UseBaseURL(string p0)
        {
            p0 = TesteAPISteps()[p0.ToLower()].ToString();

            restClient = new RestClient(p0);
            return restClient;
        }

        public RestRequest UseApi(string p0)
        {
            foreach (var variable in returnVariables)
            {
                p0 = p0.Replace($"{{{variable.Key}}}", variable.Value);
            }
            restRequest = new RestRequest(p0);
            return restRequest;
        }

        public void UseHeader(string p0, string p1)
        {
            foreach (var variable in returnVariables)
            {
                p1 = p1.Replace($"{{{variable.Key}}}", variable.Value);
            }
            restRequest.AddHeader(p0, p1);
        }

        public void UseParametro(string p0, string p1)
        {
            foreach (var variable in returnVariables)
            {
                p0 = p0.Replace($"{{{variable.Key}}}", variable.Value);
            }
            foreach (var variable in returnVariables)
            {
                p1 = p1.Replace($"{{{variable.Key}}}", variable.Value);
            }
            restRequest.AddParameter(p0, p1, ParameterType.QueryString);
        }

        public void Get()
        {
            restRequest.Method = Method.GET;
            restRequest.RequestFormat = DataFormat.Json;
        }

        public void Post()
        {
            restRequest.Method = Method.POST;
            restRequest.RequestFormat = DataFormat.Json;
        }

        public void PostPreEfetiva(string body)
        {
            UseBaseURL("URLDevPorto");
            UseApi("/api/propostas/v1/pre-efetiva");
            restRequest.Method = Method.POST;
            restRequest.RequestFormat = DataFormat.Json;
            ComBody(body);
        }
        public void PostEfetiva(string body)
        {
            UseBaseURL("URLDevPorto");
            UseApi("/api/propostas/v1/efetiva");
            restRequest.Method = Method.POST;
            restRequest.RequestFormat = DataFormat.Json;
            ComBody(body);
        }

        public void PostGarantiaChassi(string p0)
        {
            UseBaseURL("URLDevPorto");
            UseApi("/api/garantia-cdc/v1/garantia");
            restRequest.Method = Method.POST;
            restRequest.RequestFormat = DataFormat.Json;

            SaveVariavelfixa("chassi", p0);


            String multilineText = @"
		    {
                ""SEQUENCIA"": ""01"",
                ""DESCRICAO"": ""funcionando"",
                ""COMPLEMENTO"": ""alcool"",
                ""PROPOSTA"": ""{proposta}"",
                ""COR"": ""Branco"",
                ""TIPOGARANTIA"": ""Normal"",
                ""ANO"": ""2016"",
                ""MODELO"": ""Bási"",
                ""MARCA"": ""Fiat"",
                ""PLACA"": ""AQO4223"",
                ""CHASSI"": ""{chassi}"",
                ""RENAVAM"": ""12215786690""
            }";
            ComBody(multilineText);
        }

        public void PostGarantia()
        {
            //INSERE DA LANÇAMENTOS 
            _GetStoreProcedure("USP_CRIAPRIMEIROPELANCAMENTO", new Dictionary<string, object>
            {
                { "@PROPOSTA", proposta}
            });


            UseBaseURL("URLDevPorto");
            UseApi("/api/garantia-cdc/v1/garantia");
            restRequest.Method = Method.POST;
            restRequest.RequestFormat = DataFormat.Json;

            String multilineText = @"
		    {
                ""SEQUENCIA"": ""01"",
                ""DESCRICAO"": ""funcionando"",
                ""COMPLEMENTO"": ""alcool"",
                ""PROPOSTA"": ""{proposta}"",
                ""COR"": ""Branco"",
                ""TIPOGARANTIA"": ""Normal"",
                ""ANO"": ""2016"",
                ""MODELO"": ""Bási"",
                ""MARCA"": ""Fiat"",
                ""PLACA"": ""AQO4223"",
                ""CHASSI"": ""9BWZZZ377VT004251"",
                ""RENAVAM"": ""12215786690""
            }";
            ComBody(multilineText);
        }

        public void Put()
        {
            restRequest.Method = Method.PUT;
            restRequest.RequestFormat = DataFormat.Json;
        }

        public void Delete()
        {
            restRequest.Method = Method.DELETE;
            restRequest.RequestFormat = DataFormat.Json;
        }

        public void PATCH()
        {
            restRequest.Method = Method.PATCH;
            restRequest.RequestFormat = DataFormat.Json;
        }

        public void ComBody(String multilineText)
        {
            foreach (var variable in returnVariables)
            {
                multilineText = multilineText.Replace($"{{{variable.Key}}}", variable.Value);
            }

            restRequest.AddParameter(
                "application/json",
                multilineText,
                ParameterType.RequestBody);
            Console.WriteLine(JProperty.Parse(multilineText));
        }

        public IRestResponse ReturnCode(int p0)
        {
            respDB = null;

            Res = restClient.Execute(restRequest);

            if (Res.IsSuccessful)
            {
                resp = (int)Res.StatusCode;
                Assert.AreEqual(p0, resp);
            }
            else
            {
                Console.WriteLine($"{((int)Res.StatusCode).ToString()} - {Res.StatusDescription.ToString()}");
                Console.WriteLine($"{Res.Content}");
                Assert.AreEqual(p0, (int)Res.StatusCode);

                //Console.Write(Res.Content);
            }

            return Res;
        }

        public void ReturnText()
        {
            if (Res.IsSuccessful)
            {
                //return Res.StatusDescription.ToString();
                dynamic obs = JProperty.Parse(Res.Content);
                Console.WriteLine(obs.ToString());
            }
        }

        public void ReturnBD()
        {
            Console.WriteLine(respDB);
        }

        public String SaveVariavelDynamic(string p0, string p1 = null)
        {
            var routes = p0.Split('/');
            JToken obs;

            if (Res == null)
                //obs = JProperty.Parse(Res.Content);
                obs = JProperty.Parse(respDB);
            else
                //obs = JProperty.Parse(_respDB);
                obs = JProperty.Parse(Res.Content);

            JToken result = null;

            foreach (var item in routes)
            {
                if (Int32.TryParse(item, out int value))
                {
                    if (result == null)
                        result = obs[value];
                    else
                        result = result[value];
                }
                else
                {
                    if (result == null)
                        result = obs[item];
                    else
                        result = result[item];
                }
            }

            var valor = result.ToString();
            returnVariables.TryAdd(p1, valor.Trim());
            Console.WriteLine(valor);
            return valor;
        }

        public String SaveVariavelfixa(string p0, string valor)
        {
            returnVariables.TryAdd(p0, valor);
            Console.WriteLine(valor);
            return valor;
        }

        public void ComparaSeIgual2(string p0, string p1)
        {
            foreach (var variable in returnVariables)
            {
                p0 = p0.Replace($"{{{variable.Key}}}", variable.Value);
            }
            foreach (var variable in returnVariables)
            {
                p1 = p1.Replace($"{{{variable.Key}}}", variable.Value);
            }

            Assert.AreEqual(p0, p1);
        }



        public void ComparaSeIgual(string p0, string p1)
        {
            string valor = null;

            var routes = p0.Split('/');
            JToken obs;

            if (respDB == null)
                obs = JProperty.Parse(Res.Content);
            else
                obs = JProperty.Parse(respDB);

            JToken result = null;

            foreach (var item in routes)
            {
                if (Int32.TryParse(item, out int value))
                {
                    if (result == null)
                        result = obs[value];
                    else
                        result = result[value];
                }
                else
                {
                    if (result == null)
                        result = obs[item];
                    else
                        result = result[item];
                }
            }

            valor = result.ToString();

            Console.WriteLine($"{p1}, {valor}");
            Assert.AreEqual(p1, valor);
        }


        
        public void ComparaAvisoIgual(string p0)
        {
            JToken obs = JProperty.Parse(Res.Content);
            
            Assert.True(obs.ToString().Contains(p0));
        }


        public void GetStoreProcedure(string usp, Dictionary<string, object> parameters)
        {
            consulta = new Consulta_SP();

            respDB = consulta.GetUSP(usp, parameters);
        }

        private string NullOrEmpty(string parameter)
        {
            if (string.IsNullOrWhiteSpace(parameter))
                return null;
            return parameter;
        }

        public void ORetornoDaAPIDeveSer(int p0)
        {
            Res = null;
            Res = restClient.Execute(restRequest);
            if (Res.IsSuccessful)
            {
                resp = (int)Res.StatusCode;
                Assert.AreEqual(p0, resp);
            }
            else
            {
                Console.Write($"{((int)Res.StatusCode).ToString()} - {Res.StatusDescription.ToString()}");
                Console.Write(Res.Content.ToString());
                Assert.AreEqual(p0, (int)Res.StatusCode);
            }
        }

        public void ConvertXML()
        {
            string text = Res.Content;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(text);
            string node = JsonConvert.SerializeXmlNode(doc);
            dynamic obs = JProperty.Parse(node);
            Console.WriteLine(obs.ToString());
            respDB = node;
        }

        public string DadosPropostaDACASA(string proposta)
        {
            string jsonBase = new Massa_json().jsonOkDACASA();
            return AtribuiDadosProposta(proposta, jsonBase);
        }

        public string DadosPropostaLosango(string proposta)
        {
            string jsonBase = new Massa_json().jsonCheque6Parcelas();
            return AtribuiDadosProposta(proposta, jsonBase);
        }

        public string DadosPropostaEfetivacaoLosango(string proposta)
        {
            string jsonBase = new Massa_json().jsonEfetivacao();
            return AtribuiDadosProposta(proposta, jsonBase);
        }




        private string AtribuiDadosProposta(string proposta, string jsonBase)
        {
            JObject objetoJson = JObject.Parse(jsonBase);

            objetoJson["proposta"] = proposta;

            string json = JsonConvert.SerializeObject(objetoJson);
            this.proposta = proposta;
            body = json;
            return json;
        }

        public string DadosProdutoAlterado(string valor)
        {
            string jsonBase = body;
            JObject objetoJson = JObject.Parse(jsonBase);
            objetoJson["produto"] = valor;

            string json = JsonConvert.SerializeObject(objetoJson);
            body = json;
            return json;
        }

        public void _GetStoreProcedure(string usp, Dictionary<string, object> parameters)
        {
            consulta = new Consulta_SP();

            respDB = consulta.GetUSP(usp, parameters);

        }



        public void _ComparaSeIgualGerenciador(string p0, string p1)
        {
            foreach (var variable in returnVariables)
            {
                p0 = p0.Replace($"{{{variable.Key}}}", variable.Value);
            }
            foreach (var variable in returnVariables)
            {
                p1 = p1.Replace($"{{{variable.Key}}}", variable.Value);
            }

            Console.WriteLine($"{p0}, {p1}");
            Assert.AreEqual(p0, p1);
        }

        public void _ComparaSeDiferente(string p0, string p1)
        {
            foreach (var variable in returnVariables)
            {
                p0 = p0.Replace($"{{{variable.Key}}}", variable.Value);
            }
            foreach (var variable in returnVariables)
            {
                p1 = p1.Replace($"{{{variable.Key}}}", variable.Value);
            }

            Console.WriteLine($"{p0}, {p1}");
            Assert.AreNotEqual(p0, p1);
        }

        public void _PreparaBodyEfetivacao()
        {
            var body = JObject.Parse(this.body);
            body["parceiro"] = parceiro;
            body["usuario"] = usuario;
            body["senha"] = "123456";
            body["lojista"] = lojista;
            body["loja"] = loja;
            body["proposta"] = proposta;
            this.body = JsonConvert.SerializeObject(body);
        }
    }
}
