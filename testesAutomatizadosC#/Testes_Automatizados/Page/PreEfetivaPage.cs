﻿using System;
using System.Collections.Generic;
using System.Text;
using Testes_Automatizados.Page;

namespace Testes_Automatizados.Page
{
    public class PreEfetivaPage
    {
        private string _formaLiberacao;

        public PreEfetivaPage()
        {
                
        }        

        public void AtribuiFormaLiberaca(string formaLiberacao)
        {
            _formaLiberacao = formaLiberacao;
        }
    }
}
