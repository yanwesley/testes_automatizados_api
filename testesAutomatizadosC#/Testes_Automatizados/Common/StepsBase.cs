﻿using BDD.Util.Banco;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Testes_Automatizados.Common
{
    public class StepsBase
    {
        public IConfigurationRoot _configuration { get; set; }
        public SqlServerContext _conexao { get; set; }

        public StepsBase()
        {
            _configuration = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build();
            _conexao = new SqlServerContext(_configuration);
        }



    }
}
