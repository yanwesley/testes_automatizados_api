﻿#language: pt-br
Funcionalidade: InclusaoGarantiaSituacao40

@Sprint29
Cenário: Possibilitar a inclusão de garantias  quando a situação da proposta for (40 pré-efetivado)
	Dado gero uma nova proposta e deixo na situacao '21'
	Entao eu informo o valor '001062' no campo produto
	E realizo uma Pré-Efetivação
	Quando retorna o codigo 200
	E a mensagem 'Executado com sucesso'
	Entao consulta proposta confirmando situacao em 40
	E tento realizar adição de garantia
	Quando retorna o codigo 200
	Quando consulta os dados gravados da garantia