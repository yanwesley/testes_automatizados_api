﻿#language: pt-br
Funcionalidade: Valido a utilização da SP_TIPOSEGURO

Cenário:efetivo uma proposta com uma seguradora inválida
	Dado que eu tenha uma proposta na situação 40 que possua seguradora
	E verifico qual a seguradora foi apresentada para a proposta  (SEGURADORA from propostasoperacoes)
	E incluo os dados da seguradora na tabela SEGURADORASEXCLUFLOJA
	E pré-efeitvo a proposta
	Entao deve ser apresentado a seguinte mensagem de erro "Seguradora inválida para a UF da proposta"
	E por fim, excluo a seguradora da tabela SEGURADORASEXCLUFLOJA