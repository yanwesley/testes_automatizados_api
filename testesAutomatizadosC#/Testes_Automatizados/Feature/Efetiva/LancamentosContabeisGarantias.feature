﻿#language: pt-br

Funcionalidade: LancamentosContabeisGarantias

Cenario: LancamentosContabeisGarantias
    Dado que eu inicie uma proposta do parceiro 'Losango'
	E a proposta esteja "PRE-EFETIVADO"
	E grave essa proposta
	E prepare a chamada da 'Losango'
	Quando insiro a garantia
	Entao retorna o codigo 200
	Entao verifico se os regitros contabeis foram gravados