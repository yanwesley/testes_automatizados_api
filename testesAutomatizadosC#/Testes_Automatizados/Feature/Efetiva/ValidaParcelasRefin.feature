﻿#language: pt-br
Funcionalidade: ValidaParcelasRefin

@Sprint29
Cenário:Caso de sucesso ao validar numero de parcelas refin
	Dado Que tenha uma  proposta no status 40
	E produto refin
	E com numero {p0} de pacelas
	Quando Efetivar proposta
	E verifica numero de parcelas
	E se numero de parcelar for igual de {p0}
	Entao efetiva com sucesso.

Cenário:Caso de erro ao validar numero de parcelas refin
	Dado Que tenha uma  proposta no status 40
	E produto refin
	E com numero {p0} de pacelas
	Quando Efetivar proposta
	E verifica numero de parcelas
	E se numero de parcelar for diferente de {p0}
	Entao retorna mensagem "O valor do contrato ficou diferente do valor da proposta!"