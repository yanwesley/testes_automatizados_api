﻿#language: pt-Br
Funcionalidade: Verifico se o usuário apresentando possui as permissões para efetivar uma proposta

Cenario: Efetivar proposta com usuário supervisor
	Dado que eu tenha uma proposta na situação 40
	E utilize um usuário do tipo supervisor
	Quando efetivar a proposta
	Então a proposta deverá ser efetivada
	E a mensagem “Sucesso” deverá ser exibida

Cenario: Efetivar proposta com usuário não supervisor e sem permissão
	Dado que eu tenha uma proposta na situação 40
	E utilize um usuário sem permissão
	Quando efetivar a proposta
	Então a proposta deverá não deverá ser efetivada
	E a mensagem “Usuário sem permissão” deverá ser exibida

Cenario: Efetivar proposta com usuário não supervisor e com permissão
	Dado que eu tenha uma proposta na situação 40
	E utilize um usuário com permissão
	Quando efetivar a proposta
	Então a proposta deverá ser efetivada
	E a mensagem “Sucesso” deverá ser exibida