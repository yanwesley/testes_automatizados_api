﻿#language: pt-Br
Funcionalidade: ValidaDadosEfetiva

Cenario: valida se o período de efetivação já extrapolou o do intPrazoEfetivacao
	Dado que eu tenha uma proposta no status 40
	E Verifica quantidade de dias úteis entre a data de emissão e a data de movimento atual
	E valida se o periodo de efetivação e maior que o conteudo do campo intPrazoEfetivacao
	Quando Efetivo a proposta
	Então retorna a seguinte mensagem "O prazo máximo para efetivar a proposta é de (intPrazoEfetivacao)  dia(s)."

Cenario: Erro caso a moeda nao seja encontrada
	Dado que eu tenha uma proposta no status 40
	E e verifica se moeda 01 encontra se  cadastrada na tabela moedas
	E caso não esteja
	Quando Efetivo a proposta
	Então retorna a seguinte mensagem "Moeda não cadastrada!"

Cenario: Verifica vigencia da moeda
	Dado que eu tenha uma proposta no status 40
	E e verifica se há valore preenchido no campo vigencia
	E caso não tenha valor
	Quando Efetivo a proposta
	Então retorna a seguinte mensagem "Vigência Inicial desta Moeda (" & strMoeda & ") não está definida!"

Cenario: Verifica vigencia final da moeda
	Dado que eu tenha uma proposta no status 40
	E verifica se campo vigenciafinal na tabela moedas
	E caso não seja uma data
	Quando Efetivo a proposta
	Então retorna a seguinte mensagem "Vigência final desta Moeda (" & strMoeda & ") não está definida!"

Cenario: Verificar se a data de emissão é menor do que a data de vigência inicial da moeda
	Dado que eu tenha uma proposta no status 40
	E Verifica se a data de emissão é menor do que a data de vigência inicial da moeda
	Quando Efetivo a proposta
	Então retorna a seguinte mensagem "Data de emissão menor que data da Vigência Inicial desta Moeda (" & strMoeda & ")!"

Cenario: Verificar se a data de emissão é maior do que a data de vigência inicial da moeda
	Dado que eu tenha uma proposta no status 40
	E Verifica se a data de emissão é maior do que a data de vigência inicial da moeda
	Quando Efetivo a proposta
	Então retorna a seguinte mensagem "Data de emissão maior que data da Vigência Inicial desta Moeda (" & strMoeda & ")!"

Cenario: Verificar se a situação da moeda esta inativa
	Dado que eu tenha uma proposta no status 40
	E e verifica se campo situacao na tabela moedas e diferente de A
	Quando efetivo a proposta
	Então Retorna a mensagem: "A moeda (" & strMoeda & ") indicada está Inativa!"

Cenario:Se houver garantia, verificar se o modelo do carro está dentro das datas setadas como limite
	Dado que eu tenha uma proposta no status 40
	E verificar modelo do carro
	E se data estiver dentro das datas setadas como limite anoinicial e anofinal na tabela planos
	Quando efetivo a proposta
	Então Retorna a mensagem: "Ano/Modelo do Veículo não permitido para este plano!"

Cenario:Verificar se a variavel dblTAC for maior do que 0 a o tipo de pessoa for "F"
	Dado que eu tenha uma proposta no status 40
	E verifica se a variavel dblTAC e maior que zero
	E se tipo de pessoa for F
	Quando efetivo a proposta
	Então Retorna a mensagem: "Não pode haver cobrança de TAC para pessoa física!"

Cenario:Verifica se o avalista esta cadastrado na tabela clientes
	Dado que eu tenha uma proposta no status 40
	E Verifica se o avalista esta cadastrado na tabela clientes
	E se avalista nao estiver cadastrado
	Quando efetivo a proposta
	Então Retorna a mensagem: "Avalista " & i% & " não cadastrado!"

Cenario:Verifica se o avalista é o mesmo que o cliente
	Dado que eu tenha uma proposta no status 40
	E se o avalista é o mesmo que o cliente
	Quando efetivo a proposta
	Então Retorna a mensagem: "Existe um avalista cadastrado com o código do Cliente!"