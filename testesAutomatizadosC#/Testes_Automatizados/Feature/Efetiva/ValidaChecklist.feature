﻿#language: pt-br
Funcionalidade: ValidaChecklist

@Sprint29
Cenário: Ao efetivar proposta verifica se campo Upload confirmado é 'S'
	Dado que tenha uma proposta no status 40
	Quando efetivar proposta
	Entao verifico se Proposta.uploadConfirmado é igual a 'S'
	E verifico se informações foram atualizadas na tabela CHECKLIST_PROPOSTAS

Cenário: Ao efetivar proposta verifica se campo Upload confirmado é diferente de 's'
	Dado que tenha uma proposta no status 40
	Quando efetivar proposta
	Entao verifico se Proposta.uploadConfirmado = N
	E retorna mensagem "Existem pendências de documentação. Verifique para dar continuidade à proposta."

Cenário: Ao efetivar proposta verifica se campo Upload confirmado é ' '
	Dado que tenha uma proposta no status 40
	Quando efetivar proposta
	Entao verifico se Proposta.uploadConfirmado = ''
	Entao retorna mensagem "Existem pendências de documentação. Verifique para dar continuidade à proposta."

Cenário: Ao efetivar proposta verifica se campo Upload confirmado é 'NULL'
	Dado que tenha uma proposta no status 40
	Quando efetivar proposta
	Entao verifico se Proposta.uploadConfirmado = 'NULL'
	Entao retorna mensagem "Existem pendências de documentação. Verifique para dar continuidade à proposta."