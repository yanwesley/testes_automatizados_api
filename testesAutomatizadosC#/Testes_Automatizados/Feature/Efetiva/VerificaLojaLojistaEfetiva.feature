﻿#language: pt-br
Funcionalidade: VerificaLojaLojista 

@Sprint29
Cenário: Verifica se loja e lojista permanecem os mesmos  durante a efetivação
	Dado que seja gravada uma proposta com determinada '<loja>' e '<lojista>'
	Quando realizar a efetivacao da proposta ,são verificados os valores
	Entao  retorna '<Mensagem>'

	Exemplos:
		| Loja | Lojista | Mensagem                                      | 
		| 5215 | 905161  | Efetivado com sucesso                         | 
		| 1    | 905161  | Loja do usuário diferente da loja da proposta | 
		| 5215 | 1       | Lojista da proposta não é a mesma da mensagem |