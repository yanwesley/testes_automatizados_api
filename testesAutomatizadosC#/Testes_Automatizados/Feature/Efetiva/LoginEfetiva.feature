﻿#language: pt-br
Funcionalidade: Verifico as informações do usuário ao realizar o login através do EfetivaProposta

Cenario: Usuário e senha corretos
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Proposta efetivada”

Cenario: Usuário com senha incorreta
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário
	E informo a proposta
	E informe a senha incorreta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Senha não confere”

Cenario: Usuário com senha bloqueada
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Senha bloqueada”

Cenario: Usuário não informa senha
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário
	E não informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Senha invalida”

Cenario: Usuário com senha expirada
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Senha expirada”

Cenario: Usuário inválido
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário inválido
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Usuário inválido”

Cenario: Parceiro incorreto ou inexistente
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro incorreto
	E informe o usuário
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Parceiro incorreto”

Cenario: Parceiro inativo
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro com status inativo
	E informe o usuário
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Parceiro inativo”

Cenario: Parceiro não pertence ao grupo do usuário
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário que não pertença ao parceiro informado
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Parceiro incorreto para o usuário”

Cenario: Não informo o parceiro
	Dado que eu tenha uma proposta na situação 40
	E infome não informo o parceiro
	E informe o usuário
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Parceiro inválido”

Cenario: Não informo o usuário
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe não informo o usuário
	E informe a senha
	E informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Usuário inválido”

Cenario: Não informo a proposta
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário
	E informe a senha
	E não informo a proposta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Proposta é obrigatório”

Cenario: informo uma proposta incorreta
	Dado que eu tenha uma proposta na situação 40
	E infome um parceiro
	E informe o usuário
	E informe a senha
	E informo uma proposta incorreta
	Quando efetivar a proposta
	Então deve apresentar a mensagem “Proposta incorreta"