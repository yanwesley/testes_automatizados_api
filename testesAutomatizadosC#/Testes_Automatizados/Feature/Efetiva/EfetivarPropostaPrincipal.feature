﻿#language: pt-br
Funcionalidade: EfetivarPropostaPrincipal

@Sprint29
Cenário:Caso de sucesso validações de campos obrigatorios
	Dado Que tenha uma  proposta no status 40
	Quando Efetivar proposta
	Entao verifica <campos>
	E verifica <valor>
	E  retorna <mensagem>

	Exemplos:
		| campo            | valor      | mensagem              |
		| Senha            | 123456     | Executado com sucesso |
		| Proposta         | 3810403906 | Executado com sucesso |
		| Loja             | 5169       | Executado com sucesso |
		| Lojista          | 905156     | Executado com sucesso |
		| Parceiro         | 25         | Executado com sucesso |
		| Usuario          | DACASA1    | Executado com sucesso |
		| uploadConfirmado | S          | Executado com sucesso |

Cenário:Caso retorno erro se valores = null ou vazio
	Dado Que tenha uma  proposta no status 40
	Quando Efetivar proposta
	Entao verifica se <campos>
	E verifica <valor>
	E  retorna <mensagem>

	Exemplos:
		| campo            | valor | mensagem                             |
		| Senha            |       | Campo Senha não informado            |
		| Proposta         |       | Campo Proposta não informado         |
		| Loja             |       | Campo Loja não informado             |
		| Lojista          |       | Campo Lojista não informado          |
		| Parceiro         |       | Campo Parceiro não informado         |
		| Usuario          |       | Campo Usuario não informado          |
		| uploadConfirmado |       | Campo uploadConfirmado não informado |