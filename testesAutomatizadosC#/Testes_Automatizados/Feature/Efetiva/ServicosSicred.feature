﻿#language: pt-br
Funcionalidade: ServicosSicred

@Sprint29
Cenário:Erro quando o numero de liberações é maior que 10
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E numero de liberacoes seja maior que 10
	Entao retorna mensagem "Não são permitidas operações com mais de 10 liberações."

Cenário: Proposta sem informações sobre liberações
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se for operação especial
	E não conter nenhuma liberação
	Entao retorna mensagem "Operacao Especial: Obrigatorio informar as liberacoes"

Cenário:Quando nao encontrado numero de proposta
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se a proposta não foi encontrada
	Entao retorna mensagem "Proposta não encontrada!"

Cenário:numero de CHEQUES incompativel com as cadastradas
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se conter cheques mas a quantidade difere do prazo
	Entao retorna mensagem "O numero de cheques não correponde ao prazo da operação!"

Cenário:Erro quando houver cobrança  de tarifa de inclusao e  de renovação de cadastro
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se houver houver valor de inclusão de cadastro
	E Se houver houver valor de renovação de cadastro
	Entao retorna mensagem "Não pode haver cobrança simultânea de tarifa de inclusão e de renovação de cadastro!"

Cenário:Erro quando o produto nao conter nome
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se não houver produto ou o produto não conter o nome
	Entao retorna mensagem "Produto informado não encontrado!"

Cenário:Erro quando nao houver familia do produto
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se não houver a familia do produto
	Entao retorna mensagem "A familia do produto não foi encontrada!"

Cenário:Erro quando nao houver vinculo com lojista
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se o tipo pagamento for folha
	E não houver conveniada
	E ou a conveniada não conter vínculo com o lojista
	Entao retorna mensagem "Conveniada (folha) não vinculada ao lojista da operação"

Cenário:Erro quando nao houver vinculo com loja
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se o tipo pagamento for folha
	E não houver conveniada
	E ou a conveniada não conter vínculo com o loja
	Entao retorna mensagem "Conveniada (folha) não vinculada ao loja da operação"

Cenário:Erro quando cliente nao é encontrado
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se o cliente não for cadastrada ou encontrato, ou não conter o CPF
	Entao retorna mensagem "Cliente informado não está cadastrado!"

Cenário:Se for uma proposta do H2H e a classe profissional não estiver ativa
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E a classe profissional não estiver ativa
	Entao retorna mensagem "Classe profissional e profissao do cliente invalidos ou desativados!"

Cenário:Se for uma proposta do H2H e a profissão não estiver ativa
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E A profissão não estiver ativa
	Entao retorna mensagem "Profissão do cliente invalida ou desativada!"

Cenário:Erro se renda for maior que 999.999,99
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E A renda for maior que 999.999,99
	Entao retorna mensagem "Valor da renda do cliente excede 999.999,99!"

Cenário:Erro Se o tipo de pagamento for Folha e o funcionário não ser conveniado
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E o tipo de pagamento for Folha
	E o funcionário não ser conveniado
	Entao retorna mensagem "Cliente não é funcionário de uma empresa conveniada!!"

Cenário:Erro Se o tipo de pagamento for  D e o funcionário não ser conveniado
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E o tipo de pagamento for D
	E o funcionário não ser conveniado
	Entao retorna mensagem "Cliente não é funcionário de uma empresa conveniada!!"

Cenário:Erro Se houver garantia e não conter o código do veículo
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se houver garantia
	E não conter o código do veículo
	Entao retorna mensagem "Erro nas Garantias: Codigo do veículo não informado"

Cenário:Erro Se a situação da proposta não está entre as situações permitidas para a efetivação
	Dado que eu tenha uma proposta {p0}
	Quando efetivar proposta
	E Verificar status da proposta
	Entao retorna mensagem "Não foi possível efetivar a proposta {p0}. A proposta não pode ter a sua situação alterada de {1} para 20 (efetivada)! "

Cenário:Erro Se o contrato não foi encontrado
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se o contrato não foi encontrado
	Entao retorna "mensagem "Proposta não encontrada!"

Cenário:Erro Se o lojista do contrato está bloqueado
	Dado que eu tenha uma proposta no status 40 com lojista {P0}
	Quando efetivar proposta
	E Se o lojista {P0} do contrato está bloqueado
	Entao retorna "O Lojista {p0} esta com a producao bloqueada!"

Cenário:Erro Se a loja do contrato está bloqueado
	Dado que eu tenha uma proposta no status 40 com loja {P0}
	Quando efetivar proposta
	E Se o loja {P0} do contrato está bloqueado
	Entao retorna "O loja {p0} esta com a producao bloqueada!"

Cenário:Erro se o valor total PST do contrato for maior que zero e está diferente da soma dos valores de PST da proposta
	Dado que eu tenha uma proposta no status 40 com loja {P0}
	Quando efetivar proposta
	E Se o valor total PST do contrato for maior que zero
	E está diferente da soma dos valores de PST da proposta
	Entao retorna " Valor total de PST na proposta difere do somatorio dos valores dos itens de PST!"

Cenário:Erro Se alguma liberação tiver o valor zerado
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se alguma liberação tiver o valor zerado
	Entao retorna " Liberação com valor zerado!"

Cenário:Erro se não houver liberação
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E Se nao houver liberação
	Entao retorna " Dados da liberação não encontrados!"

Cenário:Erro Se o valor de compra menos valor renegociado for diferente do valor liberado
	Dado que eu tenha uma proposta no status 40
	Quando efetivar proposta
	E  Se o valor de compra menos valor renegociado for diferente do valor liberado
	Entao retorna " Inconsistencia entre os valores a liberar, da operação e das liquidações por dentro!"