﻿#language: pt-br
Funcionalidade: ValidaChaveEfetiva

@Sprint29
Cenário:caso de sucesso quando cliente estiver cadastrado na tabela clientes
	Dado que tenha uma proposta na situacao 40
	E verificado se cliente existe na tabela CLIENTES
	Quando efetivar proposta
	Entao proposta efetiva com sucesso

Cenário:Erro quando não existir cliente cadastrado na tabela clientes
	Dado que tenha uma proposta na situacao 40
	E verificado se cliente existe na tabela CLIENTES
	Quando efetivar proposta
	E nao existir cliente cadastrado
	Entao retorna mensagem "Cliente xxxx não Cadastrado!"

Cenário:Verificar se contrato está ativo
	Dado que tenha uma proposta na situacao 40
	E verificado se campo situacao na tabela contratoscdc e igual a 'A'
	E verificado se campo situacao na tabela contratoscdc e igual a 'P'
	Quando efetivar proposta
	Entao proposta efetiva com sucesso

Cenário:Verificar se contrato não ativo
	Dado que tenha uma proposta na situacao 40
	E verificado se campo situacao na tabela contratoscdc e diferente a 'A'
	E verificado se campo situacao na tabela contratoscdc e diferente a 'P'
	Quando efetivar proposta
	Entao retorna mensagem "Contrato não está ativo!"

Cenário: Verifica se o LOJISTA da proposta esta com produção bloqueada
	Dado que tenha uma proposta na situacao 40
	E verifico se campo ProducaoBloqueada na tabela lojista tem valor 'S'
	Quando efetivar a proposta
	Entao retorna a mensagem "Nao foi possivel efetivar a proposta. O Lojista " & strLojista & " esta com a producao bloqueada!"

Cenário: Verifica se o LOJISTA da proposta não esta com produção bloqueada
	Dado que tenha uma proposta na situacao 40
	E verifico se campo ProducaoBloqueada na tabela lojista tem valor diferente de 'S'
	Quando efetivar a proposta
	Entao proposta efetiva com sucesso

Cenário: Verifica se a LOJA da proposta não esta com produção bloqueada
	Dado que tenha uma proposta na situacao 40
	E verifico se campo ProducaoBloqueada na tabela lojas tem valor diferente de 'S'
	Quando efetivar a proposta
	Entao proposta efetiva com sucesso

Cenário: Verifica se a LOJA  da proposta esta com produção bloqueada
	Dado que tenha uma proposta na situacao 40
	E verifico se campo ProducaoBloqueada na tabela lojas tem valor igual  'S'
	Quando efetivar a proposta
	Entao retorna a mensagem "Nao foi possivel efetivar a proposta.A loja " & strLoja & " do lojista " & strLojista & " esta com a producao bloqueada!"