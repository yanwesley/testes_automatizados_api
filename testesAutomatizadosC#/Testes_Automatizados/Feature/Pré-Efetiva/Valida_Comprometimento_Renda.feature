﻿#language: pt-BR
Funcionalidade: Valida Comprometimento Renda
@ignore
Cenario: Valida comprometimento renda por valor de proposta
	Dado que eu inicie uma proposta do parceiro 'Losango'
	E a proposta esteja "PRE EFETIVADA"
	E com o valor de renda igual ao valor solicitado
	E grave essa proposta
	E prepare a chamada da 'Losango'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 422
	E retorna o aviso 'Comprometimento de Renda acima do limite!'
	E limpo as variaveis
@ignore
Cenario: Valida comprometimento renda por quantidade de contratos
	Dado que eu tenho uma proposta na situação '21' com contratos que comprometam a renda total
	Quando eu informo o valor '5504' no campo Loja
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 422
	E retorna a mensagem de Erro 'Contrato(s) em Aberto - Comprometimento Atual (R$): xxx,xxx,xx0.00'
@ignore
Cenario: Valida comprometimento renda por valor da prestação da proposta
	Dado que eu tenho uma proposta na situação '21' com prestacao igual a renda
	Quando eu informo o valor '5504' no campo Loja
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 422
	E retorna a mensagem de Erro 'Valor da prestação (R$): xxx,xxx,xx0.00'
@ignore
Cenario: Valida comprometimento renda por valor quantidade de prestações de contratos
	Dado que eu tenho uma proposta na situação '21' com prestacao mais de um contrato
	Quando eu informo o valor '5504' no campo Loja
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 422
	E retorna a mensagem de Erro 'Valor da prestação (R$): xxx,xxx,xx0.00'