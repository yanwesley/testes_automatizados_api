﻿#language: pt-br
Funcionalidade: GravaCheques

@Sprint28
Cenário:  validar que GravaCheque persiste no banco de dados com produto cheque e operação especial 'N'
	Dado que eu tenho uma proposta na situacao '21' com tipo de produto {p}
	E pré-efetive a proposta
	Entao retorna o codigo 200
	E verifco que as informações persistiram no banco

