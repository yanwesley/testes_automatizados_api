﻿#language: pt-br
Funcionalidade: Validar as formas de liberação

@Sprint27
Esquema do Cenario: Pré-efetivo uma proposta com diversas formas de liberação
	Dado que eu tenha uma proposta na situação '21'
	E nos dados de liberaçao informe a forma de liberação <liberacao>
	E nos dados de liberaçao informe o banco <banco>
	E nos dados de liberação informe a agência <agência>
	E nos dados de liberação informe a C/C <c/c>
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo <codigo>
	Então retorna a mensagem '<mensagem>'

	Exemplos:
		| cenário                       | liberacao | codigo | banco | agência | c/c    | mensagem                                                  |
		| Forma de liberação via doc    | 02        | 200    | 0341  | 6954    | 038706 | Executado com sucesso                                     |
		| Forma de liberação via C/C    | 03        | 200    | 0341  | 6954    | 038706 | Executado com sucesso                                     |
		| Forma de liberação via cheque | 01        | 200    | 0341  | 6954    | 038706 | Executado com sucesso                                     |
		| Outras formas liberação       | 04        | 200    | 0341  | 6954    | 038706 | Executado com sucesso                                     |
		| Forma de liberação via OP     | 05        | 200    | 0341  | 6954    | 038706 | Executado com sucesso                                     |
		| Banco inválido via doc        | 02        | 404    | 0000  | 6954    | 038706 | Liberação do Contrato via DOC: Código do Banco Inválido!  |
		| Banco vazio via doc           | 02        | 404    |       | 6954    | 038706 | Liberação do Contrato via DOC: Código do Banco Inválido!  |
		| Agência inválida via doc      | 02        | 404    | 0341  |         | 038706 | Liberação do Contrato via DOC: Agência Inválida!          |
		| C/C inválida via doc          | 02        | 404    | 0341  | 6954    |        | Liberação do Contrato via DOC: Conta Corrente Inválida!   |
		| C/C inválida via C/C          | 03        | 404    | 0341  | 6954    |        | Liberação do Contrato via Conta Corrente: Número da Conta |