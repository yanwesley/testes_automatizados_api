﻿#language: pt-br
Funcionalidade: Validar preenchimento das informações do seguro

@Sprint27
Cenário: Pré-efetivo uma proposta com o tipo de seguro 01 - básico
	Dado que eu tenha uma proposta pronta para ser pré-efetivada
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 200
	E retorna a mensagem 'Sucesso'
	Então valido que as informações do seguro persisitiram no banco

Cenário: Pré-efetivo uma proposta na situação 40 e verifico sua situação
	Dado que eu tenha uma propostas Pré-Efetivada
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna a mensagem de Erro 'Proposta não pode ser alterada, pois, já está pré-efetivada.'

