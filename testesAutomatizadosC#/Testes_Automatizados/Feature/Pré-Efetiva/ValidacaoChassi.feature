﻿#language: pt-br
Funcionalidade: ValidacaoChassi

@Sprint28
Cenário: Adiciono a garantia e chassi com 17 caracteres preenchidos
	Dado que eu inicie uma proposta do parceiro '<parceiro>'
	E a proposta esteja "PRONTA PARA PRE EFETIVAR"
	E utilize o produto '1062'
	E grave essa proposta
	E prepare a chamada da '<parceiro>'
	Quando insiro a garantia com o chassi '<chassi>'
	Entao retorna o codigo 200
	E limpo as variaveis

	Exemplos:
		| parceiro | chassi            |
		| Losango  | 9BWZZZ377VT004251 |

@Sprint28
Cenário: Adiciono a garantia e chassi com erros de preenchimento diferentes
	Dado que eu inicie uma proposta do parceiro '<parceiro>'
	E a proposta esteja "PRONTA PARA PRE EFETIVAR"
	E utilize o produto '1062'
	E grave essa proposta
	E prepare a chamada da '<parceiro>'
	Quando insiro a garantia com o chassi '<chassi>'
	Entao retorna o codigo <codigo>
	E retorna o aviso '<mensagem>'
	E limpo as variaveis

	Exemplos:
		| parceiro | chassi               | codigo | mensagem                                                |
		| Losango  | 7A7154SrW7A6L        | 400    | Tamanho Inválido                                        |
		| Losango  | 7A71 54SrW 7A6L 4190 | 400    | Chassi Invalido, Contem Espaço(s)                       |
		| Losango  | IOQ154SrW7A6L4190    | 400    | Chassi Contem Caracter Inválido                         |
		| Losango  | 7A7154SrW7AAAAAAA    | 400    | Chassi Contem Caracter Repetidos                        |
		| Losango  | 7A7154SrW7A6L41AB    | 400    | Chassi Contem Caracter invalidos nas ultimas 4 posições |
		| Losango  | 0BWZZZ377VT004251    | 400    | Chassi Inválido, Contem Primeiro Caracter como 0 (Zero) |




#cenário: adiciono a garantia e o chassi onde verifica o continente e pais de origem
#	dado que eu tenha uma proposta na situação '<situação>' com produto cdc
#	e insiro uma garantia
#	e insiro número de chassi '<chassi>'
#	quando pré-efetivar a proposta
#	entao retorna a mensagem de erro '<mensagem>'
#	e limpo as variaveis
#
#	exemplos:
#		| situação | chassi            | mensagem                                         |
#		| 21       | 707154srw7a6l4190 | pais inválido código: '+ @pos01+@pos02           |
#		| 21       | 7a7154srw7a6l4190 | continente: '@continente + ' pais: ' @paisorigem |
#
#cenario:adiciono a garantia e o chassi onde verifica o fabricante
#	dado que eu tenha uma proposta na situação '<situação>' com produto cdc
#	e insiro uma garantia
#	e insiro número de chassi '<chassi>'
#	quando pré-efetivar a proposta
#	entao retorna a mensagem de erro '<mensagem>'
#	e limpo as variaveis
#
#	exemplos:
#		| situação | chassi            | mensagem                                                            |
#		| 21       | 7a0154srw7a6l4190 | fabricante inválido código: '+ @pos01+@pos02+@pos03                 |
#		| 21       | 7a0154srw7a6l4190 | @continente + ' pais: ' @paisorigem + ' fabricante: ' + @fabricante |