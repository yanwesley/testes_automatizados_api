﻿#language: pt-br
Funcionalidade: Valido o Grava liberação

Cenário: Pré-Efetivo uma proposta na situação 21 e verifico que sua etapa foi alterada
	Dado que eu busque uma proposta pronta para pré-efetivação
	E pré-efetive a proposta
	Entao retorna o codigo 200
	E retorna a mensagem 'Executado com sucesso'
	Então a proposta deve ter seu etapa alterado para '6'