﻿#language: pt-br
Funcionalidade: FormaEnvio

#validação esta escrita no codigo mas nao entra para validação variavel compromentimentorenda nunca preenchido para validação
@Sprint29
#Cenário:  Verifica se informações persistem no banco  após a pré efetivação (E-MAIL)
#	Dado que eu inicie uma proposta do parceiro 'Losango'
#	E grave essa proposta
#	E prepare a chamada da 'Losango'
#	E forma de envio seja 'e-mail'
#	E endereço de e-mail esteja preenchido como 'wrjr@gft.com'
#	Quando realizo uma Pré-Efetivação
#	Entao retorna o codigo 200
#	E retorna a mensagem 'Executado com sucesso'

Cenário: Retorno de erro caso não atendam as regras email
	Dado que eu inicie uma proposta com parceiro'<parceiro>' e com os seguintes valores '<prazo>','<valorcompra>',<seguro>,'<dataprimeirovencimento>','<plano>','<produto>'
	E grave essa proposta
	E prepare a chamada da '<parceiro>'
	E forma de envio seja '<formaenvio>'
	E endereço de e-mail esteja preenchido como '<enderecoemail>'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 422
	E retorna o aviso '<aviso>'
	E limpo as variaveis

	Exemplos:
		| parceiro | prazo | valorcompra | seguro | dataprimeirovencimento | plano | produto | formaenvio | enderecoemail | aviso                              |
		| Losango  | 10    | 2000        | 0      | 06/04/2020             | 3561  | 001028  | correio    |               | O campo E-mail deve ser informado. |
		| Losango  | 10    | 2000        | 0      | 06/04/2020             | 3561  | 001028  | email      |               | O campo E-mail deve ser informado. |

#| Losango  | 10    | 2000        | 0      | 06/04/2020             | 3561  | 001028  |            |               | O campo E-mail deve ser informado. |
#| Losango  | 10    | 2000        | 0      | 06/04/2020             | 3561  | 001028  |            |               | O campo E-mail deve ser informado. |
#| Losango  | 10    | 2000        | 0      | 06/04/2020             | 3561  | 001028  |            |               | O campo E-mail deve ser informado. |
#Cenário: Retorno de erro caso não atendam as regras endereco_email
#	Dado que eu tenha uma proposta com cheque pronta para ser pré-efetivada
#	E Proposta.formaEnvio.endereco_email seja Null ou vazio
#	Quando realizo uma Pré-Efetivação
#	E retorna o codigo 200
#	Entao retorna a mensagem "Erro ao incluir Forma(s) de Envio para a Proposta."
#
#Cenário:  Verifica se informações persistem no banco  após a pré efetivação (CORREIO)
#	Dado que eu tenha uma proposta com cheque pronta para ser pré-efetivada
#	E Proposta.formaEnvio.correio não seja Null ou vazio
#	E Proposta.formaEnvio.correio seja "S"
#	Quando realizo uma Pré-Efetivação
#	E retorna o codigo 200
#	Entao Valido se informações persistem no Banco de dados
#
#Cenário: Retorno de erro caso não atendam as regras (correio)
#	Dado que eu tenha uma proposta com cheque pronta para ser pré-efetivada
#	E Proposta.formaEnvio.correio seja null ou vazio
#	Quando pre efetivar proposta
#	Entao retorna a mensagem "Erro ao incluir Forma(s) de Envio para a Proposta."
#
#Cenário: Verifica se informações persistem no banco  após a pré efetivação (LOJA)
#	Dado que eu tenha uma proposta com cheque pronta para ser pré-efetivada
#	E Proposta.formaEnvio.loja não seja Null ou vazio
#	E Proposta.formaEnvio.loja seja "S"
#	Quando realizo uma Pré-Efetivação
#	E retorna o codigo 200
#	Entao Valido se informações persistem no Banco de dados
#
#Cenário: Retorno de erro caso não atendam as regras (LOJA)
#	Dado que eu tenha uma proposta com cheque pronta para ser pré-efetivada
#	E Proposta.formaEnvio.loja seja null ou vazio
#	Quando realizo uma Pré-Efetivação
#	E retorna o codigo 200
#	Entao retorna a mensagem "Erro ao incluir Forma(s) de Envio para a Proposta."