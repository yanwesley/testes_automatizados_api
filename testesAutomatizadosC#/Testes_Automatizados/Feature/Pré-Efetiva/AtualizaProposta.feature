﻿#language: pt-br
Funcionalidade: Atualiza proposta

@Sprint27 
Cenário: Pré-efetivo uma proposta na situação 21 e verifico sua situação
    Dado que eu inicie uma proposta do parceiro 'Losango'
	E grave essa proposta
	E prepare a chamada da 'Losango'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 200
	E retorna a mensagem 'Executado com sucesso'
	Então consulto no banco a situação da proposta pré-efetivada
	E limpo as variaveis
	

Cenário: Pré-efetivo uma proposta na situação 40 e verifico sua situação
	Dado que eu tenha uma propostas Pré-Efetivada
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna o aviso 'Proposta não pode ser alterada, pois, já está pré-efetivada.'








	