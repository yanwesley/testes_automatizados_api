﻿#language: pt-BR
Funcionalidade: Realiza a validação de preenchimento de campos Agencia, cliente e Data de nascimento
	

Cenario: Validando data de nascimento da proposta

    Dado que eu inicie uma proposta do parceiro 'Losango'
	E altero a data nascimento para 'nulo'
	E grave essa proposta
	E prepare a chamada da 'Losango'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna o aviso 'não possui data de nascimento informada!'
	

Cenario: Validando a agencia da proposta
    Dado que eu inicie uma proposta do parceiro 'Losango'
	E altero a Agencia e deixo invalida
	E grave essa proposta
	E prepare a chamada da 'Losango'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna o aviso 'Código de agencia inválida!'