﻿#language: pt-BR
Funcionalidade: Valida Servico Sicred

Esquema do Cenario: Se não tiver Proposta devolve o erro "A PROPOSTA INFORMADA NÃO FOI LOCALIZADA"
	Dado que eu tenha uma proposta na situação '21'
	Quando eu informo o valor '<prop>' no campo Proposta
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 500
	E retorna a mensagem de Erro '<msg>'
	Exemplos: 
	| name                                           | prop       | msg                                    |
	| Teste 01 - Não informando a Proposta           |            | Proposta não informada                 |
	| Teste 02 - Informando uma Proposta inexistente | 9449878013 | Proposta não existe no banco de dados. |

Esquema do Cenario: Se a situação da proposta for diferente de 21 e a etapa for diferente de 6
	Dado que eu tenha uma proposta na situação '<situacao>'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo <cod>
	E retorna a mensagem de Erro '<msg>'
	Exemplos: 
	| name                                | situacao | cod | msg                                                          |
	| Teste 02 - Proposta com Situação 40 | 40       | 401 | Proposta não pode ser alterada, pois, já está pré-efetivada. |
	| Teste 03 - Proposta com Situação 31 | 31       | 401 | Proposta não pode ser pré-efetivada. Situação inválida.      |
	| Teste 03 - Proposta com Etapa 2     | 61       | 401 | Proposta não pode ser pré-efetivada. Situação inválida.      |
	| Teste 03 - Proposta com Etapa 3     | 99       | 401 | Proposta não pode ser pré-efetivada. Situação inválida.       |

Cenario: Se possuir cheques e a quantidade de cheques for diferente do prazo
	Dado que eu tenho uma proposta na situação '21'
	Quando eu informo o valor '3809778365' no campo Proposta
	Quando eu informo o valor '001262680108501145833000877341' no campo Cheque
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna a mensagem de Erro '[770] - O numero de cheques não correponde ao prazo da operação!'

Cenario: Se o produto não for encontrado
	Dado que eu tenho uma proposta na situação '21'
	Quando eu informo o valor '3809778374' no campo Proposta
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 422
	E retorna a mensagem de Erro 'Produto 905251 não cadastrado.'

Cenario: Se a Origem da proposta for diferente de "H" e a classe profissional e a profissão não estiverem ativas
	Dado que eu tenho uma proposta na situação '21' com origem diferente de 'H' classe profissional inativa
	Quando eu informo o valor '3809778443' no campo Proposta
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna a mensagem de Erro '[635] - Profissão do cliente invalida ou desativada!'

Cenario: Se quando o Banco do CHEQUE não estiver cadastro nas configurações do Sicred
	Dado que eu tenha uma proposta na situação '21'
	Quando eu informo o valor '3809778365' no campo Proposta
	Quando eu informo o valor '000062680108501145833000877341' no campo Cheque da parcela '4'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 422
	E retorna a mensagem de Erro 'Banco informado não existe no banco de dados.'
