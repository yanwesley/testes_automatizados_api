﻿#language: pt-br
Funcionalidade: Validar o preenchimento do CMC7 do cheque
#CMC7 base [001][2027][6][010][850140][59][2][000344524][2]

@Cmc7
Cenário: Pré-efetivo uma proposta com produto 'P' e informo a agência zerada no CMC7 
	Dado que eu possua uma proposta na situação 21 com produto 'P'
	E informo o seguinte CMC7 '001000080108501485800003445244'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna a mensagem de Erro 'Cheques: Informe corretamente o Código da Agência p/ a parcela' 

@Cmc7
Cenário: Pré-efetivo uma proposta com produto 'P' e informo a comp zerada no CMC7 
	Dado que eu possua uma proposta na situação 21 com produto 'P'
	E informo o seguinte CMC7 '001202760008501405900003445242'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 422
	E retorna a mensagem de Erro 'Cmc7 001202760008501405900003445242 é inválido.' 

@Cmc7
Cenário: Pré-efetivo uma proposta com produto 'P' e informo o número do cheque zerado no CMC7 
	Dado que eu possua uma proposta na situação 21 com produto 'P'
	E informo o seguinte CMC7 '001202770100000005900003445244'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna a mensagem de Erro 'Cheques: Informe corretamente o número do Cheque p/ a parcela' 

@Cmc7
Cenário: Pré-efetivo uma proposta com produto 'P' e informo o número da conta zerada no CMC7 
	Dado que eu possua uma proposta na situação 21 com produto 'P'
	E informo o seguinte CMC7 '001202760108501405900000000000'
	Quando realizo uma Pré-Efetivação
	Entao retorna o codigo 401
	E retorna a mensagem de Erro 'Cheques: Informe corretamente o número da Conta Corrente p/ a parcela'



